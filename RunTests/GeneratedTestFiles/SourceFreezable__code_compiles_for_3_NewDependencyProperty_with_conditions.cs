
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.None'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.None'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty NoneProperty;
#if CUSTOM
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Custom'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Custom'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CustomProperty;
#endif
#if SL3_0
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Silverlight'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Silverlight'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty SilverlightProperty;
#else
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.NotSilverlight'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.NotSilverlight'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty NotSilverlightProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.NoneProperty = DependencyProperty.Register(
                "None", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.NoneChangedShim)
                )
            );
            SourceFreezable.SilverlightProperty = DependencyProperty.Register(
                "Silverlight", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.SilverlightChangedShim)
                )
            );
#else
            SourceFreezable.NoneProperty = DependencyProperty.Register(
                "None", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int)
                )
            );
            SourceFreezable.NotSilverlightProperty = DependencyProperty.Register(
                "NotSilverlight", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int)
                )
            );
#endif
#if SL3_0 && CUSTOM
            SourceFreezable.CustomProperty = DependencyProperty.Register(
                "Custom", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.CustomChangedShim)
                )
            );
#endif
#if !SL3_0 && CUSTOM
            SourceFreezable.CustomProperty = DependencyProperty.Register(
                "Custom", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int)
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int None
        {
            get { return (int)this.GetValue(SourceFreezable.NoneProperty); }
            set { this.SetValue(SourceFreezable.NoneProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_None_RevertingDependencyPropertyChange_SL;

        private static void NoneChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_None_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_None_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.NoneProperty, e.OldValue);
                self._privateBackingFieldForProperty_None_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnSubpropertyChanged();
        }
#endif
#if CUSTOM
        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int Custom
        {
            get { return (int)this.GetValue(SourceFreezable.CustomProperty); }
            set { this.SetValue(SourceFreezable.CustomProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Custom_RevertingDependencyPropertyChange_SL;

        private static void CustomChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_Custom_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_Custom_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.CustomProperty, e.OldValue);
                self._privateBackingFieldForProperty_Custom_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnSubpropertyChanged();
        }
#endif
#endif
#if SL3_0
        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int Silverlight
        {
            get { return (int)this.GetValue(SourceFreezable.SilverlightProperty); }
            set { this.SetValue(SourceFreezable.SilverlightProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Silverlight_RevertingDependencyPropertyChange_SL;

        private static void SilverlightChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_Silverlight_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_Silverlight_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.SilverlightProperty, e.OldValue);
                self._privateBackingFieldForProperty_Silverlight_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnSubpropertyChanged();
        }
#endif
#else
        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int NotSilverlight
        {
            get { return (int)this.GetValue(SourceFreezable.NotSilverlightProperty); }
            set { this.SetValue(SourceFreezable.NotSilverlightProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_NotSilverlight_RevertingDependencyPropertyChange_SL;

        private static void NotSilverlightChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_NotSilverlight_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_NotSilverlight_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.NotSilverlightProperty, e.OldValue);
                self._privateBackingFieldForProperty_NotSilverlight_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnSubpropertyChanged();
        }
#endif
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
