
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceDependencyObject.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new UIPropertyMetadata(
                    new FreezableImpl(),
                    new PropertyChangedCallback(SourceDependencyObject.CustomChangedHandler),
                    new CoerceValueCallback(SourceDependencyObject.CustomCoerceHandler)
                ),
                new ValidateValueCallback(SourceDependencyObject.CustomValidateHandler)
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-customCallbacks.
        /// </summary>
        public FreezableImpl CustomCallbacks
        {
            get { return (FreezableImpl)this.GetValue(SourceDependencyObject.CustomCallbacksProperty); }
            set { this.SetValue(SourceDependencyObject.CustomCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL;

        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            if (self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            object value = e.NewValue;
            object coercedValue = SourceDependencyObject.CustomCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceDependencyObject.CustomCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = SourceDependencyObject.CustomValidateHandler(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceDependencyObject.CustomCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'CustomCallbacks'.", value);
            }

            object localValue = self.ReadLocalValue(CustomCallbacksProperty);
            if (object.ReferenceEquals(localValue, DependencyProperty.UnsetValue))
                e.NewValue.SafeFreeze();

            e.OldValue.SafeSubpropertyChanged(self.CustomCallbacksSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.CustomCallbacksSubpropertyChanged_SL, true);

            SourceDependencyObject.CustomChangedHandler(self, e);
        }

        private void CustomCallbacksSubpropertyChanged_SL(object sender, EventArgs e)
        {
            SourceDependencyObject.CustomChangedHandler(this, null);
        }
#endif

    }

}
