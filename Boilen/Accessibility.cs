﻿
using System;


namespace Boilen {

    /// <summary>
    /// Specifies the kinds of member accessibility.
    /// </summary>
    public enum Accessibility {

        Public = 0,
        Protected,
        Internal,
        Private,

    }

}
