
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Boilen.Freezables;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.Fill" /> dependency property. This field is read-only.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.Fill" /> dependency property.
        /// </returns>
        public static readonly DependencyProperty FillProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.FillProperty = DependencyProperty.Register(
                "Fill", typeof(Brush), typeof(SourceFreezable),
                new PropertyMetadata(
                    (Brush)null,
                    new PropertyChangedCallback(SourceFreezable.FillChangedShim)
                )
            );
#else
            SourceFreezable.FillProperty = Shape.FillProperty.AddOwner(
                typeof(SourceFreezable),
                new FrameworkPropertyMetadata(
                    (Brush)null,
                    FrameworkPropertyMetadataOptions.AffectsArrange,
                    (PropertyChangedCallback)null,
                    new CoerceValueCallback(DependencyPropertyCallbacks.CoerceNonNegative<Brush>)
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets the <see cref="System.Windows.Media.Brush" /> that specifies how the shape's interior is painted.
        /// </summary>
        /// <returns>
        /// A <see cref="System.Windows.Media.Brush" /> that describes how the shape's interior is painted. The default is <see langword='null'/>.
        /// </returns>
        public Brush Fill
        {
            get { return (Brush)this.GetValue(SourceFreezable.FillProperty); }
            set { this.SetValue(SourceFreezable.FillProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Fill_RevertingDependencyPropertyChange_SL;

        private static void FillChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_Fill_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_Fill_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.FillProperty, e.OldValue);
                self._privateBackingFieldForProperty_Fill_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.CoerceNonNegative<Brush>(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceFreezable.FillProperty, coercedValue);
                return;
            }

            e.OldValue.SafeSubpropertyChanged(self.FillSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.FillSubpropertyChanged_SL, true);

            self.OnSubpropertyChanged();
            self.InvalidateArrange();
        }

        private void FillSubpropertyChanged_SL(object sender, EventArgs e)
        {
            this.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
            this.Fill.SafeFreeze();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
