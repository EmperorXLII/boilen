﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestMetadataOverride : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_MetadataOverride( Type sourceType ) {
            var pt = Partial.Type( sourceType, typeof( Control ) )
                .AddMetadataOverride<Brush>( Control.BackgroundProperty, Brushes.Blue )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_attached_MetadataOverride( Type sourceType ) {
            var pt = Partial.Type( sourceType, typeof( Control ) )
                .AddMetadataOverride<int>( Grid.RowProperty, 1 )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_FrameworkContentElement( Type sourceType ) {
            var pt = Partial.Type( sourceType, typeof( FrameworkContentElement ) )
                ;
            var members = Compile.PartialType( pt );
        }


        public static IEnumerable<object[]> SourceTypes {
            get {
                yield return new object[] { typeof( SourceClass ) };
            }
        }

    }

}
