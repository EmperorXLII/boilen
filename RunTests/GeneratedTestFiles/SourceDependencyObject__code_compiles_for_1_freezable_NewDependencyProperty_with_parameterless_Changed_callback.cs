
using System;
using System.Windows;
using Boilen.Freezables;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceDependencyObject.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    new FreezableImpl(),
                    new PropertyChangedCallback(SourceDependencyObject.CustomCallbacksChangedShim)
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-customCallbacks.
        /// </summary>
        public FreezableImpl CustomCallbacks
        {
            get { return (FreezableImpl)this.GetValue(SourceDependencyObject.CustomCallbacksProperty); }
            set { this.SetValue(SourceDependencyObject.CustomCallbacksProperty, value); }
        }

#if SL3_0
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            object localValue = self.ReadLocalValue(CustomCallbacksProperty);
            if (object.ReferenceEquals(localValue, DependencyProperty.UnsetValue))
                e.NewValue.SafeFreeze();

            e.OldValue.SafeSubpropertyChanged(self.CustomCallbacksSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.CustomCallbacksSubpropertyChanged_SL, true);

            self.OnCustomCallbacksChanged();
        }

        private void CustomCallbacksSubpropertyChanged_SL(object sender, EventArgs e)
        {
            this.OnCustomCallbacksChanged();
        }
#else
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            self.OnCustomCallbacksChanged();
        }
#endif

    }

}
