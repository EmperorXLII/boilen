
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using System.Reflection;
using Xunit;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestLazyProperty : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_LazyProperty( Type sourceType ) {
            var pci = MemberTestInfo.Create<object>( "objProperty" );

            var pt = Partial.Type( sourceType )
                .AddLazyProperty<object>( pci.ParameterName, pci.Description, "new object()" )
                ;
            var members = Compile.PartialType( pt );

            CheckLazyProperties( members, pci );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_LazyProperty_with_external_documentation( Type sourceType ) {
            var pci = MemberTestInfo.Create<object>( "objProperty" );

            using( ChangeExternalDocumentationPrefix( ) ) {
                var pt = Partial.Type( sourceType )
                    .AddLazyProperty<object>( pci.ParameterName, "new object()" )
                    ;
                var members = Compile.PartialType( pt );

                CheckLazyProperties( members, pci );
            }
        }


        #region Utility

        public static IEnumerable<object[]> SourceTypes { get { return SourceTypesCore; } }

        private static PropertyInfo[] CheckLazyProperties( IEnumerable<MemberInfo> members, params MemberTestInfo[] propInfos ) {
            return CheckProperties( members, propInfos, property => {
                Assert.True( property.CanRead );
                Assert.True( property.CanWrite );
            } );
        }

        #endregion

    }

}
