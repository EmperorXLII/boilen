
using System;
using System.Windows;
using System.Windows.Media;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Brush'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Brush'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty BrushProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.BrushProperty = DependencyProperty.Register(
                "Brush", typeof(Brush), typeof(SourceFreezable),
                new PropertyMetadata(
                    new SolidColorBrush(Colors.Red),
                    new PropertyChangedCallback(SourceFreezable.BrushChangedShim)
                )
            );
#else
            SourceFreezable.BrushProperty = DependencyProperty.Register(
                "Brush", typeof(Brush), typeof(SourceFreezable),
                new PropertyMetadata(
                    Brushes.Red
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-brush.
        /// </summary>
        public Brush Brush
        {
            get { return (Brush)this.GetValue(SourceFreezable.BrushProperty); }
            set { this.SetValue(SourceFreezable.BrushProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Brush_RevertingDependencyPropertyChange_SL;

        private static void BrushChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_Brush_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_Brush_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.BrushProperty, e.OldValue);
                self._privateBackingFieldForProperty_Brush_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            e.OldValue.SafeSubpropertyChanged(self.BrushSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.BrushSubpropertyChanged_SL, true);

            self.OnSubpropertyChanged();
        }

        private void BrushSubpropertyChanged_SL(object sender, EventArgs e)
        {
            this.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
            this.Brush.SafeFreeze();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
