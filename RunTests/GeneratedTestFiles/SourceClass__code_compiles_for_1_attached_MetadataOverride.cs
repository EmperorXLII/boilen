
using System;
using System.Windows;
using System.Windows.Controls;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass : Control
    {

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceClass()
        {
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceClass), new FrameworkPropertyMetadata(typeof(SourceClass)));
            Grid.RowProperty.OverrideMetadata(typeof(SourceClass), new FrameworkPropertyMetadata((int)1));
#endif

            SourceClass.InitializeType();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class.
        /// </summary>
        public SourceClass()
        {
#if SL3_0
            this.DefaultStyleKey = typeof(SourceClass);
            Grid.SetRow(this, (int)1);
#endif

            this.InitializeInstance();
        }

        static partial void InitializeType();

        partial void InitializeInstance();

    }

}
