
using System;
using System.Windows;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceDependencyObject),
                new UIPropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.CustomChangedHandler),
                    new CoerceValueCallback(DependencyPropertyCallbacks.ExternalCoerceHandler)
                ),
                new ValidateValueCallback(SourceDependencyObject.CustomValidateHandler)
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-customCallbacks.
        /// </summary>
        public int CustomCallbacks
        {
            get { return (int)this.GetValue(SourceDependencyObject.CustomCallbacksProperty); }
            set { this.SetValue(SourceDependencyObject.CustomCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL;

        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            if (self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.ExternalCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceDependencyObject.CustomCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = SourceDependencyObject.CustomValidateHandler(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceDependencyObject.CustomCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'CustomCallbacks'.", value);
            }

            SourceDependencyObject.CustomChangedHandler(self, e);
        }
#endif

    }

}
