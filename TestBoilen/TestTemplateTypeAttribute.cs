
using System;
using Xunit;


namespace Boilen {

    public class TestTemplateTypeAttribute {

        [Fact]
        public void Namespace_default_is_empty( ) {
            var tt = new TemplateTypeAttribute( );

            Assert.Equal( tt.Namespace, "" );
        }

        [Fact]
        public void TemplateTypeAttribute_uses_empty_string_for_null_value( ) {
            string nullNamespace = null;
            var tt = new TemplateTypeAttribute( nullNamespace );

            Assert.Equal( tt.Namespace, "" );
        }

        [Fact]
        public void TemplateTypeAttribute_uses_specified_Namespace_value( ) {
            string expectedNamespace = "Test.Namespace";
            var tt = new TemplateTypeAttribute( expectedNamespace );

            Assert.Equal( tt.Namespace, expectedNamespace );
        }

    }

}
