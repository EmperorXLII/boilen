﻿
using System.Reflection;
using System.Runtime.InteropServices;


[assembly: AssemblyProduct( "boilen" )]
[assembly: AssemblyCompany( "https://bitbucket.org/EmperorXLII/boilen" )]
[assembly: AssemblyCopyright( "Copyright © 2009" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

#if DEBUG
[assembly: AssemblyConfiguration( "Debug" )]
#else
[assembly: AssemblyConfiguration( "Release" )]
#endif

[assembly: ComVisible( false )]
