
using System;
using System.Windows;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new UIPropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.CustomChangedHandler),
                    new CoerceValueCallback(DependencyPropertyCallbacks.ExternalCoerceHandler)
                ),
                new ValidateValueCallback(SourceGenericDependencyObject<T>.CustomValidateHandler)
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-customCallbacks.
        /// </summary>
        public int CustomCallbacks
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL;

        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            if (self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.ExternalCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = SourceGenericDependencyObject<T>.CustomValidateHandler(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'CustomCallbacks'.", value);
            }

            SourceGenericDependencyObject<T>.CustomChangedHandler(self, e);
        }
#endif

    }

}
