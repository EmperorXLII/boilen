
using System;
using System.Windows;
using System.Windows.Shapes;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.StrokeDashOffset" /> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.StrokeDashOffset" /> dependency property.
        /// </returns>
        public static readonly DependencyProperty StrokeDashOffsetProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.StrokeDashOffsetProperty = DependencyProperty.Register(
                "StrokeDashOffset", typeof(double), typeof(SourceFreezable),
                new PropertyMetadata(
                    (double)0,
                    new PropertyChangedCallback(SourceFreezable.StrokeDashOffsetChangedShim)
                )
            );
#else
            SourceFreezable.StrokeDashOffsetProperty = Shape.StrokeDashOffsetProperty.AddOwner(
                typeof(SourceFreezable),
                new UIPropertyMetadata(
                    (double)0,
                    new PropertyChangedCallback(SourceFreezable.CustomChangedHandler),
                    new CoerceValueCallback(SourceFreezable.CustomCoerceHandler)
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets a <see cref="System.Double" /> that specifies the distance within the dash pattern where a dash begins.
        /// </summary>
        /// <returns>
        /// A <see cref="System.Double" /> that represents the distance within the dash pattern where a dash begins.
        /// </returns>
        public double StrokeDashOffset
        {
            get { return (double)this.GetValue(SourceFreezable.StrokeDashOffsetProperty); }
            set { this.SetValue(SourceFreezable.StrokeDashOffsetProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_StrokeDashOffset_RevertingDependencyPropertyChange_SL;

        private static void StrokeDashOffsetChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_StrokeDashOffset_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_StrokeDashOffset_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.StrokeDashOffsetProperty, e.OldValue);
                self._privateBackingFieldForProperty_StrokeDashOffset_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            object value = e.NewValue;
            object coercedValue = SourceFreezable.CustomCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceFreezable.StrokeDashOffsetProperty, coercedValue);
                return;
            }

            SourceFreezable.CustomChangedHandler(self, e);
            self.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
