
using System;
using System.Windows;
using System.Windows.Media;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Brush'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Brush'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty BrushProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.BrushProperty = DependencyProperty.Register(
                "Brush", typeof(Brush), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    new SolidColorBrush(Colors.Red)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.BrushProperty = DependencyProperty.Register(
                "Brush", typeof(Brush), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    Brushes.Red
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-brush.
        /// </summary>
        public Brush Brush
        {
            get { return (Brush)this.GetValue(SourceDependencyObject.BrushProperty); }
            set { this.SetValue(SourceDependencyObject.BrushProperty, value); }
        }

    }

}
