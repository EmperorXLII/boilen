
using System;
using System.Windows;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.StandardCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.StandardCallbacks'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty StandardCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.StandardCallbacksProperty = DependencyProperty.Register(
                "StandardCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.StandardCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.StandardCallbacksProperty = DependencyProperty.Register(
                "StandardCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new FrameworkPropertyMetadata(
                    default(int),
                    FrameworkPropertyMetadataOptions.AffectsArrange,
                    (PropertyChangedCallback)null,
                    new CoerceValueCallback(DependencyPropertyCallbacks.CoerceNonNegative<int>)
                ),
                new ValidateValueCallback(DependencyPropertyCallbacks.IsValidEnum<int>)
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-standardCallbacks.
        /// </summary>
        public int StandardCallbacks
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL;

        private static void StandardCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            if (self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.CoerceNonNegative<int>(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = DependencyPropertyCallbacks.IsValidEnum<int>(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'StandardCallbacks'.", value);
            }

            self.InvalidateArrange();
        }
#endif

    }

}
