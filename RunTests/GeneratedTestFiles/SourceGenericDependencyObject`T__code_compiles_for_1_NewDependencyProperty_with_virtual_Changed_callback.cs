
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.VirtualCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.VirtualCallbacks'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty VirtualCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.VirtualCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.VirtualCallbacksChangedShim)
                )
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-virtualCallbacks.
        /// </summary>
        public int VirtualCallbacks
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.VirtualCallbacksProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.VirtualCallbacksProperty, value); }
        }

        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            self.OnVirtualCallbacksChanged((int)e.OldValue, (int)e.NewValue);
        }

        /// <summary>
        /// Called when the <see cref="SourceGenericDependencyObject{T}.VirtualCallbacks" /> property changes.
        /// </summary>
        /// <param name='oldValue'>
        /// Old value of the <see cref="SourceGenericDependencyObject{T}.VirtualCallbacks" /> property.
        /// </param>
        /// <param name='newValue'>
        /// New value of the <see cref="SourceGenericDependencyObject{T}.VirtualCallbacks" /> property.
        /// </param>
        protected virtual void OnVirtualCallbacksChanged(int oldValue, int newValue)
        {
            this.OnVirtualCallbacksChanged();
        }

        partial void OnVirtualCallbacksChanged();

    }

}
