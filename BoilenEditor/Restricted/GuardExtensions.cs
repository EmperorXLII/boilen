﻿
using BoilenEditor.Primitives;
using System;


namespace BoilenEditor.Restricted {

    internal static class GuardExtensions {

        public static Guard<T> GuardParam<T>( this T value, string paramName ) {
            return new Guard<T>( value, paramName );
        }

        public static Guard<T> NotNull<T>( this Guard<T> guard )
            where T : class {
            if( !guard.Value.HasValue( ) )
                throw new ArgumentNullException( guard.Description );
            return guard;
        }

    }

}
