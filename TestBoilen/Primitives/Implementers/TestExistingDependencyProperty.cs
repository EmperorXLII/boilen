﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestExistingDependencyProperty : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ExistingDependencyProperty( Type sourceType ) {
            var pt = Partial.Type( sourceType )
                .AddDependencyProperty( Shape.FillProperty, p => p.Replacements
                    .Add( "shape", "test-documentation-update" )
                )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ExistingDependencyProperty_with_external_documentation( Type sourceType ) {
            using( ChangeExternalDocumentationPrefix( ) ) {
                var pt = Partial.Type( sourceType )
                    .AddDependencyProperty( Shape.FillProperty )
                    ;
                var members = Compile.PartialType( pt );
            }
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ExistingDependencyProperty_using_validation( Type sourceType ) {
            var pt = Partial.Type( sourceType )
                .AddDependencyProperty( StackPanel.OrientationProperty, p => p.Replacements
                    .Add( "orient", "test-documentation-update" )
                )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_Brush_ExistingDependencyProperty( Type sourceType ) {
            var pt = Partial.Type( sourceType )
                .AddDependencyProperty<Brush>( Shape.FillProperty, p => p
                    .SetDefaultValue( Brushes.Red )
                )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ExistingDependencyProperty_with_standard_callbacks( Type sourceType ) {
            var pt = Partial.Type( sourceType )
                .AddDependencyProperty<Brush>( Shape.FillProperty, p => p
                    .SetOptions( FrameworkPropertyMetadataOptions.AffectsArrange )
                    .SetCoerce( Coerce.NonNegative )
                )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ExistingDependencyProperty_with_change_callback( Type sourceType ) {
            var pt = Partial.Type( sourceType )
                .AddDependencyProperty<Brush>( Shape.FillProperty, p => p
                    .SetChanged( Changed.Static )
                )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ExistingDependencyProperty_with_custom_callbacks( Type sourceType ) {
            var pt = Partial.Type( sourceType )
                .AddDependencyProperty<double>( Shape.StrokeDashOffsetProperty, p => p
                    .SetChanged( "CustomChangedHandler" )
                    .SetCoerce( "CustomCoerceHandler" )
                )
                ;
            var members = Compile.PartialType( pt );
        }


        public static IEnumerable<object[]> SourceTypes {
            get {
                yield return new object[] { typeof( SourceDependencyObject ) };
                yield return new object[] { typeof( SourceGenericDependencyObject<> ) };
                yield return new object[] { typeof( SourceFreezable ) };
            }
        }

    }

}
