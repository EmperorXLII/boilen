
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit.Extensions;


namespace Boilen.Primitives.Members {

    public class TestBlockMember {

        [Theory]
        [InlineData( true )]
        [InlineData( false )]
        public void Write_succeeds_for_valid_arguments( bool writeName ) {
            string name = "test name";
            string value = "test value";
            Action<ICodeWriter> content = ( w ) => w.WriteLine( value );
            var expectedLines = new List<string>( );
            if( writeName )
                expectedLines.Add( name );
            expectedLines.Add( "{" );
            expectedLines.Add( "    " + value );
            expectedLines.Add( "}" );
            expectedLines.Add( "" );
            var writer = new StringBuilderCodeWriter( );
            var bm = new BlockMember( name, content ) { WriteName = writeName };

            bm.Write( writer );

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }

        [Theory]
        [InlineData( true )]
        [InlineData( false )]
        public void Write_succeeds_for_valid_guards( bool writeName ) {
            string name = "test name";
            string value = "test value";
            Doc doc = new Doc( "fqn", "name", "type", "parent type" );
            string guardName = "GuardMethod";
            string guardValue = "value";
            Type exceptionType = typeof( Exception );
            string docSummary = "summary";
            string guardKind = "Param";
            Action<ICodeWriter> content = ( w ) => w.WriteLine( value );
            string[] allExpectedLines = new[] {
                name,
                "{",
                "    " + guardValue + ".Guard" + guardKind + "(\"" + guardValue + "\")",
                "        ." + guardName + "();",
                "",
                "    " + value,
                "}",
                ""
            };
            var expectedLines = writeName ? allExpectedLines : allExpectedLines.Skip( 1 );
            var writer = new StringBuilderCodeWriter( );
            var guard = new Guard( doc, guardName, guardValue, exceptionType, docSummary );
            var bm = new BlockMember( name, content ) { WriteName = writeName }.AddGuards( new[] { guard } );

            bm.Write( writer );

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }

    }

}
