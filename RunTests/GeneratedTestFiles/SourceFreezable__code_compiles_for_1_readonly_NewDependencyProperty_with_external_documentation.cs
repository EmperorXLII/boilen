
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

#if SL3_0
        private static readonly DependencyProperty StatePropertyKey;

        internal static readonly DependencyProperty StateProperty;
#else
        private static readonly DependencyPropertyKey StatePropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.State'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.State'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StateProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.StatePropertyKey = DependencyProperty.Register(
                "State", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.StateChangedShim)
                )
            );
            SourceFreezable.StateProperty = StatePropertyKey;
#else
            SourceFreezable.StatePropertyKey = DependencyProperty.RegisterReadOnly(
                "State", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int)
                )
            );
            SourceFreezable.StateProperty = StatePropertyKey.DependencyProperty;
#endif

            SourceFreezable.InitializeType();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceFreezable'/> class.
        /// </summary>
        public SourceFreezable()
        {
            this.State = new int();

            this.InitializeInstance();
        }

        static partial void InitializeType();

        partial void InitializeInstance();


        /// <include file='prefix/SourceFreezable.xml' path='/doc/member[@name="State"]/*' />
        public int State
        {
            get { return (int)this.GetValue(SourceFreezable.StateProperty); }
            protected set { this.SetValue(SourceFreezable.StatePropertyKey, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL;

        private static void StateChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.StatePropertyKey, e.OldValue);
                self._privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
