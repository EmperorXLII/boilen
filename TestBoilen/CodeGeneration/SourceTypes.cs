﻿


// Used for testing code generation.
namespace Boilen.Primitives.CodeGeneration {

    public partial class SourceClass { }

    public partial class SourceDependencyObject { }

    public partial class SourceGenericDependencyObject<T> { }

    public partial class SourceFreezableBase { }

    public partial class SourceFreezable { }

    public partial struct SourceStruct { }

    public partial struct SourceStruct<T> { }

    public partial class BaseClass { }

}
