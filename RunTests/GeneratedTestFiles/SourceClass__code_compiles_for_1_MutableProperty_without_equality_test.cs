
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private int[] _privateBackingFieldForProperty_IntArrayProperty;


        /// <summary>
        /// Gets or sets test-description-for-intArrayProperty.
        /// </summary>
        public int[] IntArrayProperty
        {
            get { return this._privateBackingFieldForProperty_IntArrayProperty; }
            set
            {
                this.IntArrayPropertyCoerce(ref value);

                this.IntArrayPropertyChanging(value);
                int[] oldValue = this._privateBackingFieldForProperty_IntArrayProperty;
                this._privateBackingFieldForProperty_IntArrayProperty = value;
                this.IntArrayPropertyChanged(oldValue);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IntArrayPropertyCoerce(ref int[] value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IntArrayPropertyChanging(int[] newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IntArrayPropertyChanged(int[] oldValue);

    }

}
