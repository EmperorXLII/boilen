
using System;
using System.Collections.Generic;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct :
        IEquatable<SourceStruct>
    {

        private readonly string _privateBackingFieldForProperty_First;
        private readonly string _privateBackingFieldForProperty_Second;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified first and second.
        /// </summary>
        /// <param name='first'>
        /// Test-description-for-first.
        /// </param>
        /// <param name='second'>
        /// Test-description-for-second.
        /// </param>
        public SourceStruct(string first, string second)
        {
            this._privateBackingFieldForProperty_First = first;
            this._privateBackingFieldForProperty_Second = second;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-first.
        /// </summary>
        public string First
        {
            get { return this._privateBackingFieldForProperty_First; }
        }

        /// <summary>
        /// Gets test-description-for-second.
        /// </summary>
        public string Second
        {
            get { return this._privateBackingFieldForProperty_Second; }
        }


        #region IEquatable<SourceStruct> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the current object is equal to the <paramref name="other" /> parameter; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='other'>
        /// An object to compare with this object.
        /// </param>
        public bool Equals(SourceStruct other)
        {
            return EqualityComparer<string>.Default.Equals(this.First, other.First)
                && EqualityComparer<string>.Default.Equals(this.Second, other.Second);
        }

        /// <summary>
        /// Determines whether two specified <see cref='SourceStruct'/> objects have the same value.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the value of <paramref name='left'/> is the same as the value of <paramref name='right'/>; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='left'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        /// <param name='right'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        public static bool operator ==(SourceStruct left, SourceStruct right)
        {
            return EqualityComparer<SourceStruct>.Default.Equals(left, right);
        }

        /// <summary>
        /// Determines whether two specified <see cref='SourceStruct'/> objects have different values.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the value of <paramref name='left'/> is the same as the value of <paramref name='right'/>; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='left'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        /// <param name='right'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        public static bool operator !=(SourceStruct left, SourceStruct right)
        {
            return !EqualityComparer<SourceStruct>.Default.Equals(left, right);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the specified object  is equal to the current object; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='obj'>
        /// The object to compare with the current object.
        /// </param>
        public override bool Equals(object obj)
        {
            return obj is SourceStruct
                && this.Equals((SourceStruct)obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        public override int GetHashCode()
        {
            int hash = EqualityComparer<string>.Default.GetHashCode(this.First);
            hash = ((hash << 5) + hash) ^ EqualityComparer<string>.Default.GetHashCode(this.Second);
            return hash;
        }

        #endregion

    }

}
