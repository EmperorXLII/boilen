
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Fill'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Fill'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty FillProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.FillProperty = DependencyProperty.Register(
                "Fill", typeof(Brush), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    (Brush)null
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.FillProperty = Shape.FillProperty.AddOwner(typeof(SourceDependencyObject));
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <include file='prefix/SourceDependencyObject.xml' path='/doc/member[@name="Fill"]/*' />
        public Brush Fill
        {
            get { return (Brush)this.GetValue(SourceDependencyObject.FillProperty); }
            set { this.SetValue(SourceDependencyObject.FillProperty, value); }
        }

    }

}
