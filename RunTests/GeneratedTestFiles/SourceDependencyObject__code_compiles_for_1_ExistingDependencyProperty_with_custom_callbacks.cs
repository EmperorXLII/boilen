
using System;
using System.Windows;
using System.Windows.Shapes;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.StrokeDashOffset" /> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.StrokeDashOffset" /> dependency property.
        /// </returns>
        public static readonly DependencyProperty StrokeDashOffsetProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.StrokeDashOffsetProperty = DependencyProperty.Register(
                "StrokeDashOffset", typeof(double), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    (double)0,
                    new PropertyChangedCallback(SourceDependencyObject.StrokeDashOffsetChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.StrokeDashOffsetProperty = Shape.StrokeDashOffsetProperty.AddOwner(
                typeof(SourceDependencyObject),
                new UIPropertyMetadata(
                    (double)0,
                    new PropertyChangedCallback(SourceDependencyObject.CustomChangedHandler),
                    new CoerceValueCallback(SourceDependencyObject.CustomCoerceHandler)
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets a <see cref="System.Double" /> that specifies the distance within the dash pattern where a dash begins.
        /// </summary>
        /// <returns>
        /// A <see cref="System.Double" /> that represents the distance within the dash pattern where a dash begins.
        /// </returns>
        public double StrokeDashOffset
        {
            get { return (double)this.GetValue(SourceDependencyObject.StrokeDashOffsetProperty); }
            set { this.SetValue(SourceDependencyObject.StrokeDashOffsetProperty, value); }
        }

#if SL3_0
        private static void StrokeDashOffsetChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            object value = e.NewValue;
            object coercedValue = SourceDependencyObject.CustomCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceDependencyObject.StrokeDashOffsetProperty, coercedValue);
                return;
            }

            SourceDependencyObject.CustomChangedHandler(self, e);
        }
#endif

    }

}
