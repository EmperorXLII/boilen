
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.CustomCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.CustomCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.CustomCallbacksChangedShim)
                )
            );
#else
            SourceFreezable.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.CustomCallbacksChangedShim)
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-customCallbacks.
        /// </summary>
        public int CustomCallbacks
        {
            get { return (int)this.GetValue(SourceFreezable.CustomCallbacksProperty); }
            set { this.SetValue(SourceFreezable.CustomCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL;
#endif

#if SL3_0
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.CustomCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_CustomCallbacks_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnCustomCallbacksChanged((int)e.OldValue, (int)e.NewValue);
            self.OnSubpropertyChanged();
        }
#else
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            self.OnCustomCallbacksChanged((int)e.OldValue, (int)e.NewValue);
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
