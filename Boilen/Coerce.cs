﻿
using System;


namespace Boilen {

    /// <summary>
    /// Specifies the pre-defined kinds of property value coercion.
    /// </summary>
    public enum Coerce {

        None = 0,
        Custom,
        NonNegative,

    }

}
