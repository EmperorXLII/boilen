﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestInsertions : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_all_insertions( Type sourceType ) {
            var pci = MemberTestInfo.Create<int>( "intProperty", 3 );

            var pt = Partial.Type( sourceType );
            foreach( InsertionPoint p in Enum.GetValues( typeof( InsertionPoint ) ) )
                pt.InsertText( p, "/*" + p + "*/" );

            var members = Compile.PartialType( pt );
        }


        public static IEnumerable<object[]> SourceTypes { get { return SourceTypesCore; } }

    }

}
