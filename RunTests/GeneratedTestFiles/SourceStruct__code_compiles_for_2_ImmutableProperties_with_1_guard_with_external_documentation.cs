
using System;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;
        private readonly string _privateBackingFieldForProperty_StringProperty;


        /// <include file='prefix/SourceStruct.xml' path='/doc/member[@name="SourceStruct(int,string)"]/*' />
        public SourceStruct(int intProperty, string stringProperty)
        {
            stringProperty.GuardParam("stringProperty")
                .NotNull();

            this._privateBackingFieldForProperty_IntProperty = intProperty;
            this._privateBackingFieldForProperty_StringProperty = stringProperty;

            this.InitializeInstance();
        }

        /// <include file='prefix/SourceStruct.xml' path='/doc/member[@name="SourceStruct(int)"]/*' />
        public SourceStruct(int intProperty)
            : this(intProperty, "default") { }

        partial void InitializeInstance();


        /// <include file='prefix/SourceStruct.xml' path='/doc/member[@name="IntProperty"]/*' />
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

        /// <include file='prefix/SourceStruct.xml' path='/doc/member[@name="StringProperty"]/*' />
        public string StringProperty
        {
            get { return this._privateBackingFieldForProperty_StringProperty; }
        }

    }

}
