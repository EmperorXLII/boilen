﻿
using BoilenEditor.Primitives;
using BoilenEditor.Properties;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace BoilenEditor {

    public partial class SettingsWindow : Window {

        private readonly SettingsData settingsData_;


        public SettingsWindow( Window owner ) {
            InitializeComponent( );

            this.settingsData_ = new SettingsData( new Settings( ) );

            this.Owner = owner;
            this.DataContext = this.settingsData_;

            this.settingsListBox_.Items.SortDescriptions.Add( new SortDescription( "Name", ListSortDirection.Ascending ) );
        }


        private void SaveCanExecute( object sender, CanExecuteRoutedEventArgs e ) {
            bool isDirty =
                   this.settingsData_.HasValue( )
                && this.settingsData_.Properties.Any( p => p.IsDirty );
            if( isDirty )
                e.CanExecute = true;
        }

        private void SaveExecuted( object sender, ExecutedRoutedEventArgs e ) {
            this.settingsData_.Save( );

            App.Active.UpdateFontSize( this.settingsData_.Settings.FontSize );

            this.DialogResult = true;
        }

        private void CancelExecuted( object sender, ExecutedRoutedEventArgs e ) {
            this.Close( );
        }

        protected override void OnClosed( EventArgs e ) {
            base.OnClosed( e );

            Settings.Default.Reload( );
        }

    }

}
