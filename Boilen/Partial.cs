﻿
using Boilen.Primitives;
using System;


namespace Boilen {

    /// <summary>
    /// Provides helper methods for constructing <see cref="PartialType"/> instances.
    /// </summary>
    public static class Partial {

        /// <summary>
        /// Creates a new <see cref="PartialType"/> with the specified type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The <see cref="System.Type"/> to extend.</typeparam>
        public static PartialType Type<T>( ) { return Partial.Type( typeof( T ) ); }

        /// <summary>
        /// Creates a new <see cref="PartialType"/> with the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">The <see cref="System.Type"/> to extend.</param>
        public static PartialType Type( Type type ) { return new PartialType( type ); }

        /// <summary>
        /// Creates a new <see cref="PartialType"/> derived from type <typeparamref name="T"/> with the specified <paramref name="name"/>.
        /// </summary>
        /// <typeparam name="T">The <see cref="System.Type"/> to derive from.</typeparam>
        /// <param name="name">The name of the derived type to create.</param>
        public static PartialType Type<T>( string name ) { return Partial.Type( name, typeof( T ) ); }

        /// <summary>
        /// Creates a new <see cref="PartialType"/> derived from <paramref name="baseType"/> with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">The name of the derived type to create.</param>
        /// <param name="baseType">The <see cref="System.Type"/> to derive from.</param>
        public static PartialType Type( string name, Type baseType ) { return new PartialType( name, baseType ); }

        /// <summary>
        /// Creates a new <see cref="PartialType"/> with the specified type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The <see cref="System.Type"/> to extend.</typeparam>
        /// <param name="baseType">The <see cref="System.Type"/> to derive from.</param>
        public static PartialType Type<T>( Type baseType ) { return Partial.Type( typeof( T ), baseType ); }

        /// <summary>
        /// Creates a new <see cref="PartialType"/> with the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">The <see cref="System.Type"/> to extend.</param>
        /// <param name="baseType">The <see cref="System.Type"/> to derive from.</param>
        public static PartialType Type( Type type, Type baseType ) { return new PartialType( type, baseType ); }

        /// <summary>
        /// Creates a new <see cref="PartialType"/> with the specified type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The <see cref="System.Type"/> to extend.</typeparam>
        /// <param name="baseType">The <see cref="System.Type"/> to derive from.</param>
        /// <param name="baseType">The <see cref="System.Type"/> to derive from.</param>
        /// <param name="silverlightBaseTypeName">The full name of the <see cref="System.Type"/> to derive from in silverlight.</param>
        public static PartialType Type<T>( Type baseType, string silverlightBaseTypeName ) { return Partial.Type( typeof( T ), baseType, silverlightBaseTypeName ); }

        /// <summary>
        /// Creates a new <see cref="PartialType"/> with the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">The <see cref="System.Type"/> to extend.</param>
        /// <param name="baseType">The <see cref="System.Type"/> to derive from.</param>
        /// <param name="silverlightBaseTypeName">The full name of the <see cref="System.Type"/> to derive from in silverlight.</param>
        public static PartialType Type( Type type, Type baseType, string silverlightBaseTypeName ) { return new PartialType( type, baseType, silverlightBaseTypeName ); }

    }

}
