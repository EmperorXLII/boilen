
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial class DerivedClass : BaseClass
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;


        /// <inheritdoc/>
        /// <summary>
        /// Initializes a new instance of the <see cref='DerivedClass'/> class with the specified requiredProperty, optionalProperty, and intProperty.
        /// </summary>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public DerivedClass(int requiredProperty, int optionalProperty, int intProperty)
            : base(requiredProperty, optionalProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;

            this.InitializeInstance();
        }

        /// <inheritdoc/>
        /// <summary>
        /// Initializes a new instance of the <see cref='DerivedClass'/> class with the specified requiredProperty and optionalProperty.
        /// </summary>
        public DerivedClass(int requiredProperty, int optionalProperty)
            : this(requiredProperty, optionalProperty, -1) { }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

    }

}
