﻿
using System.Collections.Generic;
using System.Linq;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for a member with a type and modifiers.
    /// </summary>
    public abstract class ModifiedTypedMember : TypedMember {

        private readonly List<AttributeMember> attributes_ = new List<AttributeMember>( );


        /// <summary>
        /// Gets or sets field modifiers (i.e. "public", "static").
        /// </summary>
        public string Modifiers { get; set; }

        /// <summary>
        /// Gets the collection of attribute members used by the accessor.
        /// </summary>
        public IList<AttributeMember> Attributes { get { return this.attributes_; } }


        /// <summary>
        /// Initializes a new <see cref="ModifiedTypedMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the member.</param>
        /// <param name="type">The type of the member.</param>
        /// <param name="modifiers">The modifiers on the member.</param>
        protected ModifiedTypedMember( string name, string type, string modifiers )
            : this( name, type, true, modifiers ) { }

        /// <summary>
        /// Initializes a new <see cref="ModifiedTypedMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the member.</param>
        /// <param name="type">The type of the member.</param>
        /// <param name="isTypeRequired">Indicates whether <paramref name="type"/> must be set.</param>
        /// <param name="modifiers">The modifiers on the member.</param>
        protected ModifiedTypedMember( string name, string type, bool isTypeRequired, string modifiers )
            : base( name, type, isTypeRequired ) {
            this.Modifiers = modifiers ?? "";
        }


        /// <summary>
        /// Adds the specified attributes to the <see cref="Attributes"/> collection.
        /// </summary>
        public void AddAttributes( IEnumerable<AttributeMember> attributes ) {
            this.attributes_.AddRange( attributes );
        }

        /// <inheritdoc/>
        protected override void PreWrite( ICodeWriter writer ) {
            base.PreWrite( writer );
            if( this.Attributes.Any( ) )
                WriteMembers( writer, this.Attributes );
        }

    }

}
