﻿


namespace Boilen.Primitives.Members {

    public sealed class WriteNameMember : Member {

        public override Extensions UsedExtensions { get { return Extensions.None; } }

        public WriteNameMember( string name )
            : base( name ) { }

        protected override void WriteCore( ICodeWriter writer ) {
            writer.WriteLine( this.Name );
        }

    }

}
