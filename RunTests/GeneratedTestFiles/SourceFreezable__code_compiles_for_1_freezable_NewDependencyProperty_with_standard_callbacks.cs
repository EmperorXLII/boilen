
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.StandardCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.StandardCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StandardCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.StandardCallbacksProperty = DependencyProperty.Register(
                "StandardCallbacks", typeof(FreezableImpl), typeof(SourceFreezable),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceFreezable.StandardCallbacksChangedShim)
                )
            );
#else
            SourceFreezable.StandardCallbacksProperty = DependencyProperty.Register(
                "StandardCallbacks", typeof(FreezableImpl), typeof(SourceFreezable),
                new FrameworkPropertyMetadata(
                    new FreezableImpl(),
                    FrameworkPropertyMetadataOptions.AffectsArrange,
                    (PropertyChangedCallback)null,
                    new CoerceValueCallback(DependencyPropertyCallbacks.CoerceNonNegative<FreezableImpl>)
                ),
                new ValidateValueCallback(DependencyPropertyCallbacks.IsNotNull<FreezableImpl>)
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-standardCallbacks.
        /// </summary>
        public FreezableImpl StandardCallbacks
        {
            get { return (FreezableImpl)this.GetValue(SourceFreezable.StandardCallbacksProperty); }
            set { this.SetValue(SourceFreezable.StandardCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL;

        private static void StandardCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.StandardCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.CoerceNonNegative<FreezableImpl>(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceFreezable.StandardCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = DependencyPropertyCallbacks.IsNotNull<FreezableImpl>(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.StandardCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'StandardCallbacks'.", value);
            }

            e.OldValue.SafeSubpropertyChanged(self.StandardCallbacksSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.StandardCallbacksSubpropertyChanged_SL, true);

            self.OnSubpropertyChanged();
            self.InvalidateArrange();
        }

        private void StandardCallbacksSubpropertyChanged_SL(object sender, EventArgs e)
        {
            this.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
            this.StandardCallbacks.SafeFreeze();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
