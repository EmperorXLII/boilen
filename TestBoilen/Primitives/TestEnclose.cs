
using Xunit;


namespace Boilen.Primitives {

    public class TestEnclose {

        [Fact]
        public void Indent_does_not_write_to_output( ) {
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.Indent( writer ) ) { }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, StringBuilderCodeWriter.EmptyLines );
        }

        [Fact]
        public void Indent_applies_four_space_indent_to_written_values( ) {
            string value = "test value";
            string[] expectedLines = new[] { "    " + value };
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.Indent( writer ) ) {
                writer.Write( value );
            }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }


        [Fact]
        public void Braces_encloses_written_values_in_curly_braces_and_indent( ) {
            string value = "test value";
            string[] expectedLines = new[] {
                "{",
                "    " + value, "}",
                "" 
            };
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.Braces( writer ) ) {
                writer.WriteLine( value );
            }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }


        [Fact]
        public void Parenthesis_encloses_written_values_in_parenthesis( ) {
            string value = "test value";
            string[] expectedLines = new[] { "(" + value + ")" };
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.Parenthesis( writer ) ) {
                writer.Write( value );
            }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }


        [Fact]
        public void Brackets_encloses_written_values_in_square_brackets( ) {
            string value = "test value";
            string[] expectedLines = new[] { "[" + value + "]" };
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.Brackets( writer ) ) {
                writer.Write( value );
            }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }


        [Fact]
        public void NewLine_encloses_written_values_in_newlines( ) {
            string value = "test value";
            string[] expectedLines = new[] { "", value, "" };
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.NewLine( writer ) ) {
                writer.Write( value );
            }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }


        [Fact]
        public void Region_encloses_written_values_in_region_tags( ) {
            string name = "test name";
            string value = "test value";
            string[] expectedLines = new[] {
                "#region " + name,
                "",
                value,
                "",
                "#endregion",
                ""
            };
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.Region( writer, name ) ) {
                writer.WriteLine( value );
            }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }


        [Fact]
        public void Format_encloses_written_values_in_specified_formatted_string( ) {
            string openFormat = "open {0}";
            string closeFormat = "close {0}";
            string formatArg = "format arg";
            string value = "test value";
            string[] expectedLines = new[] {
                string.Format( openFormat, formatArg ),
                value,
                string.Format( closeFormat, formatArg ),
                ""
            };
            var writer = new StringBuilderCodeWriter( );

            using( Enclose.Format( writer, openFormat, closeFormat, formatArg ) ) {
                writer.WriteLine( value );
            }

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }

    }

}
