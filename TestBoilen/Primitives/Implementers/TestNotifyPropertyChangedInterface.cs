﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestNotifyPropertyChangedInterface : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_NotifyPropertyChanged( Type sourceType ) {
            var pci = MemberTestInfo.Create<object>( "mutable" );

            var pt = Partial.Type( sourceType )
                .AddMutableProperty<object>( pci.ParameterName, pci.Description )
                .ImplementINotifyPropertyChanged( )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_NotifyPropertyChanged_with_external_documentation( Type sourceType ) {
            var pci = MemberTestInfo.Create<object>( "mutable" );

            using( ChangeExternalDocumentationPrefix( ) ) {
                var pt = Partial.Type( sourceType )
                    .AddMutableProperty<object>( pci.ParameterName )
                    .ImplementINotifyPropertyChanged( )
                    ;
                var members = Compile.PartialType( pt );
            }
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_conditional_NotifyPropertyChanged( Type sourceType ) {
            var pci = MemberTestInfo.Create<object>( "mutable" );

            var pt = Partial.Type( sourceType )
                .AddMutableProperty<object>( pci.ParameterName, pci.Description )
                .ImplementINotifyPropertyChanged( i => i
                    .Condition = new CompilationSymbol( "DEBUG", false )
                )
                ;
            var members = Compile.PartialType( pt );
        }


        public static IEnumerable<object[]> SourceTypes { get { return SourceTypesCore; } }

    }

}
