﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestConstantProperty : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ConstantProperty( Type sourceType ) {
            var pci = MemberTestInfo.Create<int>( "intProperty", 3 );

            var pt = Partial.Type( sourceType )
                .AddConstantProperty<int>( pci.ParameterName, pci.Description, pci.DefaultValue )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_ConstantProperty_with_external_documentation( Type sourceType ) {
            var pci = MemberTestInfo.Create<int>( "intProperty", 3 );

            using( ChangeExternalDocumentationPrefix( ) ) {
                var pt = Partial.Type( sourceType )
                    .AddConstantProperty<int>( pci.ParameterName, pci.DefaultValue )
                    ;
                var members = Compile.PartialType( pt );
            }
        }


        public static IEnumerable<object[]> SourceTypes { get { return SourceTypesCore; } }

    }

}
