﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Windows;
using Xunit;


namespace Boilen.Primitives.Implementers {

    public class TestFreezableImplementer : TestImplementers {

        [Fact]
        public void code_compiles_for_Freezable_type( ) {
            Type sourceType = typeof( SourceFreezableBase );
            var eci = MemberTestInfo.Create<int>( "state" );

            var pt = Partial.Type( sourceType, typeof( Freezable ), typeof( FrameworkElement ).FullName )
                .AddDependencyProperty<int>( eci.ParameterName, eci.Description )
                ;
            var members = Compile.PartialType( pt );
        }

        [Fact]
        public void code_compiles_for_Freezable_type_with_external_documentation( ) {
            Type sourceType = typeof( SourceFreezableBase );
            var eci = MemberTestInfo.Create<int>( "state" );

            using( ChangeExternalDocumentationPrefix( ) ) {
                var pt = Partial.Type( sourceType, typeof( Freezable ), typeof( FrameworkElement ).FullName )
                    .AddDependencyProperty<int>( eci.ParameterName )
                    ;
                var members = Compile.PartialType( pt );
            }
        }

        [Fact]
        public void code_compiles_for_interface_on_Freezable( ) {
            var pci = MemberTestInfo.Create<object>( "mutable" );

            var pt = Partial.Type<SourceFreezableBase>( typeof( Freezable ) )
                .AddMutableProperty<object>( pci.ParameterName, pci.Description )
                .ImplementINotifyPropertyChanged( )
                ;
            var members = Compile.PartialType( pt );
        }

    }

}
