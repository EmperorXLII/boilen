﻿
using Boilen.Primitives.Members;
using System;
using System.Collections.Generic;


namespace Boilen.Primitives.Implementers {

    /// <summary>
    /// Implements some aspect of a type, such as a property or an interface implementation.
    /// </summary>
    public interface IImplementer {

        /// <summary>
        /// Gets the parent type of the implementer.
        /// </summary>
        PartialType Parent { get; }

        /// <summary>
        /// Gets the name of the implementer.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the name of the implementer member.
        /// </summary>
        string MemberName { get; }

        /// <summary>
        /// Gets the type of the implementer.
        /// </summary>
        Type Type { get; }

        /// <summary>
        /// Gets the type name of the implementer.
        /// </summary>
        string TypeName { get; }

        /// <summary>
        /// Gets the description of the implementer.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the implementation members for the implementer.
        /// </summary>
        IEnumerable<Member> Members { get; }

        /// <summary>
        /// Gets the collection of documentation primitives to add to the documentation of each member.
        /// </summary>
        Doc.ReplacementsDictionary Replacements { get; }

        /// <summary>
        /// Gets or sets the conditional compilation symbol for the implementer.
        /// </summary>
        CompilationSymbol Condition { get; set; }


        /// <summary>
        /// Called before a <see cref="PartialType"/> is <see cref="PartialType.Run"/>.
        /// </summary>
        void Prepare( );

    }

}
