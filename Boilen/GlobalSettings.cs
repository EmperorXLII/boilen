﻿
using Boilen.Primitives;
using System;


namespace Boilen {

    /// <summary>
    /// Provides helper methods for constructing <see cref="PartialType"/> instances.
    /// </summary>
    public static class GlobalSettings {

        /// <summary>
        /// Gets or sets the symbol used to identify the Silverlight runtime.
        /// </summary>
        public static string SilverlightSymbol { get; set; }

        /// <summary>
        /// Gets or sets the sub-namespace containing the <see cref="Guard"/> implementation.
        /// </summary>
        public static string ValidationSubNamespace { get; set; }

        /// <summary>
        /// Gets or sets the sub-namespace containing the dependency property extensions implementation.
        /// </summary>
        public static string DependencyPropertySubNamespace { get; set; }

        /// <summary>
        /// Gets or sets the sub-namespace containing the freezable extensions implementation.
        /// </summary>
        public static string FreezableSubNamespace { get; set; }

        /// <summary>
        /// Gets or sets the namespace containing custom code analysis rules.
        /// </summary>
        public static string CodeAnalysisRuleNamespace { get; set; }

        /// <summary>
        /// Gets or sets the name of a custom code analysis rule for weak events.
        /// </summary>
        public static string WeakEventCodeAnalysisRule { get; set; }

        /// <summary>
        /// Gets or sets the path prefix for the external documentation xml file.
        /// </summary>
        public static string ExternalDocumentationPrefix { get; set; }

        static GlobalSettings( ) {
            SilverlightSymbol = "SL3_0";
            FreezableSubNamespace = DependencyPropertySubNamespace = ValidationSubNamespace = "Restricted";
        }

    }

}
