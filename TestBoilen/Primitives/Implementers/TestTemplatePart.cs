﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestTemplatePart : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_TemplatePart( Type sourceType ) {
            var eci = MemberTestInfo.Create<Grid>( "LayoutRoot" );

            var pt = Partial.Type( sourceType, typeof( Control ) )
                .AddTemplatePart<Grid>( eci.MemberName, eci.Description )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_TemplatePart_with_external_documentation( Type sourceType ) {
            var eci = MemberTestInfo.Create<Grid>( "LayoutRoot" );

            using( ChangeExternalDocumentationPrefix( ) ) {
                var pt = Partial.Type( sourceType, typeof( Control ) )
                    .AddTemplatePart<Grid>( eci.MemberName )
                    ;
                var members = Compile.PartialType( pt );
            }
        }


        public static IEnumerable<object[]> SourceTypes {
            get {
                yield return new object[] { typeof( SourceClass ) };
            }
        }

    }

}
