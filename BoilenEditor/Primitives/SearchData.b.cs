﻿
// <auto-generated>
//   !!! WARNING !!!
//   !!!   This code was automatically generated from a T4 template.
//   !!!   Do not edit this file directly. Changes to this file will be lost.
//   !!! WARNING !!!
//   
//      Source Template - SearchData.b.tt
// </auto-generated>


using System;


namespace BoilenEditor.Primitives
{

    partial class SearchData
    {

        private readonly string _privateBackingFieldForProperty_Find;
        private readonly string _privateBackingFieldForProperty_Replace;
        private readonly bool _privateBackingFieldForProperty_Next;
        private readonly bool _privateBackingFieldForProperty_CaseSensitive;
        private readonly bool _privateBackingFieldForProperty_UseRegex;
        private readonly bool _privateBackingFieldForProperty_Wrap;
        private readonly bool _privateBackingFieldForProperty_All;


        /// <summary>
        /// Initializes a new instance of the <see cref='SearchData'/> class with the specified find, replace, next, caseSensitive, useRegex, wrap, and all.
        /// </summary>
        /// <param name='find'>
        /// The string being searched for.
        /// </param>
        /// <param name='replace'>
        /// The string to replace it with, or <see langword='null'/> for a find operation.
        /// </param>
        /// <param name='next'>
        /// A value indicating whether to search for the next match or the previous match.
        /// </param>
        /// <param name='caseSensitive'>
        /// A value indicating whether the search is case sensitive.
        /// </param>
        /// <param name='useRegex'>
        /// A value indicating whether the search should use a regular expression to perform the match.
        /// </param>
        /// <param name='wrap'>
        /// A value indicating whether the search should wrap around after it reaches the end of the document.
        /// </param>
        /// <param name='all'>
        /// A value indicating whether the search should be performed on all matches.
        /// </param>
        public SearchData(string find, string replace, bool next, bool caseSensitive, bool useRegex, bool wrap, bool all)
        {
            this._privateBackingFieldForProperty_Find = find;
            this._privateBackingFieldForProperty_Replace = replace;
            this._privateBackingFieldForProperty_Next = next;
            this._privateBackingFieldForProperty_CaseSensitive = caseSensitive;
            this._privateBackingFieldForProperty_UseRegex = useRegex;
            this._privateBackingFieldForProperty_Wrap = wrap;
            this._privateBackingFieldForProperty_All = all;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets the string being searched for.
        /// </summary>
        public string Find
        {
            get { return this._privateBackingFieldForProperty_Find; }
        }

        /// <summary>
        /// Gets the string to replace it with, or <see langword='null'/> for a find operation.
        /// </summary>
        public string Replace
        {
            get { return this._privateBackingFieldForProperty_Replace; }
        }

        /// <summary>
        /// Gets a value indicating whether to search for the next match or the previous match.
        /// </summary>
        public bool Next
        {
            get { return this._privateBackingFieldForProperty_Next; }
        }

        /// <summary>
        /// Gets a value indicating whether the search is case sensitive.
        /// </summary>
        public bool CaseSensitive
        {
            get { return this._privateBackingFieldForProperty_CaseSensitive; }
        }

        /// <summary>
        /// Gets a value indicating whether the search should use a regular expression to perform the match.
        /// </summary>
        public bool UseRegex
        {
            get { return this._privateBackingFieldForProperty_UseRegex; }
        }

        /// <summary>
        /// Gets a value indicating whether the search should wrap around after it reaches the end of the document.
        /// </summary>
        public bool Wrap
        {
            get { return this._privateBackingFieldForProperty_Wrap; }
        }

        /// <summary>
        /// Gets a value indicating whether the search should be performed on all matches.
        /// </summary>
        public bool All
        {
            get { return this._privateBackingFieldForProperty_All; }
        }

    }

}
