
using System;
using System.Windows;
using Boilen.Freezables;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(FreezableImpl), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(FreezableImpl), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    new FreezableImpl(),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.CustomCallbacksChangedShim)
                )
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-customCallbacks.
        /// </summary>
        public FreezableImpl CustomCallbacks
        {
            get { return (FreezableImpl)this.GetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty, value); }
        }

#if SL3_0
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            object localValue = self.ReadLocalValue(CustomCallbacksProperty);
            if (object.ReferenceEquals(localValue, DependencyProperty.UnsetValue))
                e.NewValue.SafeFreeze();

            e.OldValue.SafeSubpropertyChanged(self.CustomCallbacksSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.CustomCallbacksSubpropertyChanged_SL, true);

            self.OnCustomCallbacksChanged();
        }

        private void CustomCallbacksSubpropertyChanged_SL(object sender, EventArgs e)
        {
            this.OnCustomCallbacksChanged();
        }
#else
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            self.OnCustomCallbacksChanged();
        }
#endif

    }

}
