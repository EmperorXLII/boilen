
using System;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;
        private readonly string _privateBackingFieldForProperty_StringProperty;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified intProperty and stringProperty.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='stringProperty'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        /// <param name='stringProperty'>
        /// Test-description-for-stringProperty.
        /// </param>
        public SourceStruct(int intProperty, string stringProperty)
        {
            stringProperty.GuardParam("stringProperty")
                .NotNull();

            this._privateBackingFieldForProperty_IntProperty = intProperty;
            this._privateBackingFieldForProperty_StringProperty = stringProperty;

            this.InitializeInstance();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified intProperty.
        /// </summary>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public SourceStruct(int intProperty)
            : this(intProperty, "default") { }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

        /// <summary>
        /// Gets test-description-for-stringProperty.
        /// </summary>
        public string StringProperty
        {
            get { return this._privateBackingFieldForProperty_StringProperty; }
        }

    }

}
