
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.None'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.None'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty NoneProperty;
#if CUSTOM
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Custom'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Custom'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CustomProperty;
#endif
#if SL3_0
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Silverlight'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Silverlight'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty SilverlightProperty;
#else
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.NotSilverlight'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.NotSilverlight'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty NotSilverlightProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
            SourceDependencyObject.NoneProperty = DependencyProperty.Register(
                "None", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
#if CUSTOM
            SourceDependencyObject.CustomProperty = DependencyProperty.Register(
                "Custom", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
#endif
#if SL3_0
            SourceDependencyObject.SilverlightProperty = DependencyProperty.Register(
                "Silverlight", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.NotSilverlightProperty = DependencyProperty.Register(
                "NotSilverlight", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <include file='prefix/SourceDependencyObject.xml' path='/doc/member[@name="None"]/*' />
        public int None
        {
            get { return (int)this.GetValue(SourceDependencyObject.NoneProperty); }
            set { this.SetValue(SourceDependencyObject.NoneProperty, value); }
        }
#if CUSTOM
        /// <include file='prefix/SourceDependencyObject.xml' path='/doc/member[@name="Custom"]/*' />
        public int Custom
        {
            get { return (int)this.GetValue(SourceDependencyObject.CustomProperty); }
            set { this.SetValue(SourceDependencyObject.CustomProperty, value); }
        }
#endif
#if SL3_0
        /// <include file='prefix/SourceDependencyObject.xml' path='/doc/member[@name="Silverlight"]/*' />
        public int Silverlight
        {
            get { return (int)this.GetValue(SourceDependencyObject.SilverlightProperty); }
            set { this.SetValue(SourceDependencyObject.SilverlightProperty, value); }
        }
#else
        /// <include file='prefix/SourceDependencyObject.xml' path='/doc/member[@name="NotSilverlight"]/*' />
        public int NotSilverlight
        {
            get { return (int)this.GetValue(SourceDependencyObject.NotSilverlightProperty); }
            set { this.SetValue(SourceDependencyObject.NotSilverlightProperty, value); }
        }
#endif

    }

}
