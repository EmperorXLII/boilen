
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.StandardCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.StandardCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StandardCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.StandardCallbacksProperty = DependencyProperty.Register(
                "StandardCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceDependencyObject.StandardCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.StandardCallbacksProperty = DependencyProperty.Register(
                "StandardCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new FrameworkPropertyMetadata(
                    new FreezableImpl(),
                    FrameworkPropertyMetadataOptions.AffectsArrange,
                    (PropertyChangedCallback)null,
                    new CoerceValueCallback(DependencyPropertyCallbacks.CoerceNonNegative<FreezableImpl>)
                ),
                new ValidateValueCallback(DependencyPropertyCallbacks.IsNotNull<FreezableImpl>)
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-standardCallbacks.
        /// </summary>
        public FreezableImpl StandardCallbacks
        {
            get { return (FreezableImpl)this.GetValue(SourceDependencyObject.StandardCallbacksProperty); }
            set { this.SetValue(SourceDependencyObject.StandardCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL;

        private static void StandardCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            if (self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.CoerceNonNegative<FreezableImpl>(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceDependencyObject.StandardCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = DependencyPropertyCallbacks.IsNotNull<FreezableImpl>(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceDependencyObject.StandardCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_StandardCallbacks_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'StandardCallbacks'.", value);
            }

            object localValue = self.ReadLocalValue(StandardCallbacksProperty);
            if (object.ReferenceEquals(localValue, DependencyProperty.UnsetValue))
                e.NewValue.SafeFreeze();

            self.InvalidateArrange();
        }
#endif

    }

}
