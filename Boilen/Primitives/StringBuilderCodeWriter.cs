﻿
using System;
using System.Text;


namespace Boilen.Primitives {

    public sealed class StringBuilderCodeWriter : CodeWriter {

        public static readonly string[] EmptyLines = new[] { "" };


        private readonly StringBuilder writer_ = new StringBuilder( );


        public override string ToString( ) {
            return this.writer_.ToString( );
        }

        public string[] GetLines( ) {
            return this.ToString( ).Split( new[] { Environment.NewLine }, StringSplitOptions.None );
        }


        protected override void WriteCore( string value ) {
            this.writer_.Append( value );
        }

        protected override void WriteLineCore( string value ) {
            this.writer_.AppendLine( value );
        }

    }

}
