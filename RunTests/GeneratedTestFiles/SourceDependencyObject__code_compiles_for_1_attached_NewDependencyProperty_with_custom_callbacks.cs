
using System;
using System.Windows;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> attached property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> attached property.
        /// </returns>
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.RegisterAttached(
                "CustomCallbacks", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.RegisterAttached(
                "CustomCallbacks", typeof(int), typeof(SourceDependencyObject),
                new UIPropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.CustomChangedHandler),
                    new CoerceValueCallback(SourceDependencyObject.CustomCoerceHandler)
                ),
                new ValidateValueCallback(SourceDependencyObject.CustomValidateHandler)
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks" /> attached property for a specified <see cref="System.Windows.UIElement" />.
        /// </summary>
        /// <returns>
        /// The <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks" /> property value for the element.
        /// </returns>
        /// <remarks>
        /// Gets test-description-for-customCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element from which the property value is read.
        /// </param>
        public static int GetCustomCallbacks(UIElement element)
        {
            element.GuardParam("element")
                .NotNull();

            return (int)element.GetValue(SourceDependencyObject.CustomCallbacksProperty);
        }

        /// <summary>
        /// Sets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks" /> attached property to a specified element.
        /// </summary>
        /// <remarks>
        /// Sets test-description-for-customCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element to which the attached property is written.
        /// </param>
        /// <param name='value'>
        /// The needed <see cref="int" /> value.
        /// </param>
        public static void SetCustomCallbacks(UIElement element, int value)
        {
            element.GuardParam("element")
                .NotNull();

            element.SetValue(SourceDependencyObject.CustomCallbacksProperty, value);
        }
#if SL3_0
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            object value = e.NewValue;
            object coercedValue = SourceDependencyObject.CustomCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                d.SetValue(SourceDependencyObject.CustomCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = SourceDependencyObject.CustomValidateHandler(value);
            if (!isValid)
            {
                d.SetValue(SourceDependencyObject.CustomCallbacksProperty, e.OldValue);
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'CustomCallbacks'.", value);
            }

            SourceDependencyObject.CustomChangedHandler(d, e);
        }
#endif

    }

}
