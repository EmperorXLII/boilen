﻿
using System;
using System.Diagnostics;
using System.Text;


namespace BoilenEditor.Primitives.Snippets {

    [DebuggerDisplay( "ArgumentData: Name={Name}, Type={TypeName}, Value={Value}, Format={Format}" )]
    public partial class ArgumentData {

        public bool IsDescription {
            get { return this.Name.OrdinalEqual( "Description" ); }
        }


        public StringBuilder AppendTo( StringBuilder sb ) {
            string value = Ex.GetValueString( this.Value, this.Type );
            return sb.Append( value );
        }


        private static string GetTypeName( Type type ) {
            return type.GetTypeName( );
        }

    }

}
