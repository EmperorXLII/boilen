﻿
using System;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for a constant declaration.
    /// </summary>
    public sealed class ConstantMember : ModifiedTypedMember {

        private readonly string value_;


        /// <inheritdoc/>
        public override Extensions UsedExtensions { get { return Extensions.None; } }

        /// <summary>
        /// Gets the value of the constant.
        /// </summary>
        public string Value { get { return this.value_; } }


        /// <summary>
        /// Initializes a new <see cref="ConstantMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the constant.</param>
        /// <param name="type">The type of the constant.</param>
        /// <param name="value">The value of the constant.</param>
        public ConstantMember( string name, string type, string value )
            : base( name, type, "public const" ) {
            Ensure.NotNullOrEmpty( value );

            this.value_ = value;

            this.SpaceOutput = true;
        }


        /// <inheritdoc/>
        protected override void WriteCore( ICodeWriter writer ) {
            writer.WriteLine( "{0} {1} {2} = {3};", this.Modifiers, this.Type, this.Name, this.Value );
        }

    }

}
