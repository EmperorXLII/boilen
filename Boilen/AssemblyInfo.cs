﻿
using System.Reflection;


[assembly: AssemblyTitle( "boilen" )]
[assembly: AssemblyDescription( "Used to generate boilerplate code for properties, interfaces, and constructors." )]

[assembly: AssemblyVersion( "3.5.0.0" )]
[assembly: AssemblyFileVersion( "3.5.0.0" )]
