
using System;
using System.Windows;
using System.Windows.Controls;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Orientation" /> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Orientation" /> dependency property.
        /// </returns>
        public static readonly DependencyProperty OrientationProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.OrientationProperty = DependencyProperty.Register(
                "Orientation", typeof(Orientation), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    Orientation.Vertical,
                    new PropertyChangedCallback(SourceDependencyObject.OrientationChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.OrientationProperty = StackPanel.OrientationProperty.AddOwner(typeof(SourceDependencyObject));
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets a value that indicates the dimension by which child elements are stacked.
        /// </summary>
        /// <returns>
        /// The <see cref="System.Windows.Controls.Orientation" /> of child content.
        /// </returns>
        public Orientation Orientation
        {
            get { return (Orientation)this.GetValue(SourceDependencyObject.OrientationProperty); }
            set { this.SetValue(SourceDependencyObject.OrientationProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL;

        private static void OrientationChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            if (self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL)
                return;

            object value = e.NewValue;
            bool isValid = DependencyPropertyCallbacks.IsValidEnum<Orientation>(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceDependencyObject.OrientationProperty, e.OldValue);
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'Orientation'.", value);
            }
        }
#endif

    }

}
