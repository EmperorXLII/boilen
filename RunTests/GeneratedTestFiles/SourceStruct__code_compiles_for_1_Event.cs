
using System;
using System.Threading;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private EventHandler _privateBackingFieldForEvent_State;


        /// <summary>
        /// Occurs when test-description-for-state.
        /// </summary>
        public event EventHandler State
        {
            add { this._privateBackingFieldForEvent_State += value; }
            remove { this._privateBackingFieldForEvent_State -= value; }
        }

        /// <summary>
        /// Raises the <see cref='SourceStruct.State'/> event.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='e'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='e'>
        /// An instance of <see cref='EventArgs'/> that contains the event data.
        /// </param>
        private void OnState(EventArgs e)
        {
            e.GuardParam("e")
                .NotNull();

            EventHandler handler = Interlocked.CompareExchange(ref this._privateBackingFieldForEvent_State, null, null);
            if (!object.ReferenceEquals(handler, null))
            {
                handler(this, e);
            }
        }

    }

}
