﻿
using System;


namespace Boilen.Primitives {

    /// <summary>
    /// Specifies the extension namespaces used by a member.
    /// </summary>
    [Flags]
    public enum Extensions {

        None = 0,
        Validation = 1 << 0,
        DependencyProperties = 1 << 1,
        Freezable = 1 << 2,

    }

}
