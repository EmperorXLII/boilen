
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.Fill" /> dependency property. This field is read-only.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.Fill" /> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty FillProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.FillProperty = DependencyProperty.Register(
                "Fill", typeof(Brush), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    (Brush)null,
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.FillChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.FillProperty = Shape.FillProperty.AddOwner(
                typeof(SourceGenericDependencyObject<T>),
                new FrameworkPropertyMetadata(
                    (Brush)null,
                    FrameworkPropertyMetadataOptions.AffectsArrange,
                    (PropertyChangedCallback)null,
                    new CoerceValueCallback(DependencyPropertyCallbacks.CoerceNonNegative<Brush>)
                )
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets the <see cref="System.Windows.Media.Brush" /> that specifies how the shape's interior is painted.
        /// </summary>
        /// <returns>
        /// A <see cref="System.Windows.Media.Brush" /> that describes how the shape's interior is painted. The default is <see langword='null'/>.
        /// </returns>
        public Brush Fill
        {
            get { return (Brush)this.GetValue(SourceGenericDependencyObject<T>.FillProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.FillProperty, value); }
        }

#if SL3_0
        private static void FillChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.CoerceNonNegative<Brush>(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceGenericDependencyObject<T>.FillProperty, coercedValue);
                return;
            }

            self.InvalidateArrange();
        }
#endif

    }

}
