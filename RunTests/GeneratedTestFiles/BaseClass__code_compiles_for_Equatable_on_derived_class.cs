
using System;
using System.Collections.Generic;


namespace Boilen.Primitives.CodeGeneration
{

    partial class DerivedClass : BaseClass,
        IEquatable<DerivedClass>
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;


        /// <inheritdoc/>
        /// <summary>
        /// Initializes a new instance of the <see cref='DerivedClass'/> class with the specified requiredProperty, optionalProperty, and intProperty.
        /// </summary>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public DerivedClass(int requiredProperty, int optionalProperty, int intProperty)
            : base(requiredProperty, optionalProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;

            this.InitializeInstance();
        }

        /// <inheritdoc/>
        /// <summary>
        /// Initializes a new instance of the <see cref='DerivedClass'/> class with the specified requiredProperty and optionalProperty.
        /// </summary>
        public DerivedClass(int requiredProperty, int optionalProperty)
            : this(requiredProperty, optionalProperty, -1) { }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }


        #region IEquatable<DerivedClass> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the current object is equal to the <paramref name="other" /> parameter; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='other'>
        /// An object to compare with this object.
        /// </param>
        public virtual bool Equals(DerivedClass other)
        {
            return base.Equals(other)
                && EqualityComparer<int>.Default.Equals(this.IntProperty, other.IntProperty);
        }

        /// <inheritdoc/>
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public override bool Equals(BaseClass other)
        {
            return this.Equals(other as DerivedClass);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        public override int GetHashCode()
        {
            int hash = base.GetHashCode();
            hash = ((hash << 5) + hash) ^ EqualityComparer<int>.Default.GetHashCode(this.IntProperty);
            return hash;
        }

        #endregion

    }

}
