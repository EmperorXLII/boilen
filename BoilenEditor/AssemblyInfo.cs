﻿
using System.Reflection;
using System.Windows;


[assembly: AssemblyTitle( "Boilen Editor" )]
[assembly: AssemblyDescription( "Used to edit Boilen T4 files." )]

[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

[assembly: ThemeInfo( ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly )]
