
using System;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified intProperty.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='intProperty'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public SourceStruct(int intProperty)
        {
            intProperty.GuardParam("intProperty")
                .NotNull();

            this._privateBackingFieldForProperty_IntProperty = intProperty;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

    }

}
