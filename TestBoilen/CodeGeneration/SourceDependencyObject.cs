﻿
using Boilen.Freezables;
using System.Windows;


// Used for testing code generation.
namespace Boilen.Primitives.CodeGeneration {

#if SL3_0
    using DPCEA = System.Nullable<DependencyPropertyChangedEventArgs>;
#else
    using DPCEA = DependencyPropertyChangedEventArgs;
#endif

    public partial class SourceDependencyObject : FrameworkElement {

        private void OnCustomCallbacksChanged( ) { }

        private void OnCustomCallbacksChanged( int oldValue, int newValue ) { }

        private void OnCustomCallbacksChanged( FreezableImpl oldValue, FreezableImpl newValue ) { }

        private static void CustomChangedHandler( DependencyObject d, DPCEA e ) { }

        private static object CustomCoerceHandler( DependencyObject d, object baseValue ) { return baseValue; }

        private static bool CustomValidateHandler( object value ) { return true; }

        private static void FillChanged( DependencyObject d, DPCEA e ) { }

        private static object FillCoerce( DependencyObject d, object baseValue ) { return baseValue; }

    }

}
