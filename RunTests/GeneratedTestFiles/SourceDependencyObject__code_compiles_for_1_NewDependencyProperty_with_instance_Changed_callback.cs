
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.CustomCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.CustomCallbacksProperty = DependencyProperty.Register(
                "CustomCallbacks", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.CustomCallbacksChangedShim)
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-customCallbacks.
        /// </summary>
        public int CustomCallbacks
        {
            get { return (int)this.GetValue(SourceDependencyObject.CustomCallbacksProperty); }
            set { this.SetValue(SourceDependencyObject.CustomCallbacksProperty, value); }
        }

        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            self.OnCustomCallbacksChanged((int)e.OldValue, (int)e.NewValue);
        }

    }

}
