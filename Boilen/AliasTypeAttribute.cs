﻿
using Boilen.Primitives;
using System;


namespace Boilen {

    /// <summary>
    /// Marks a type alias defined in a template file.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface )]
    public sealed class AliasTypeAttribute : Attribute {

        /// <summary>
        /// The type being aliased.
        /// </summary>
        public Type Alias { get; private set; }


        /// <summary>
        /// Initializes a new <see cref="AliasTypeAttribute"/> instance.
        /// </summary>
        /// <param name="nmspace">The type being aliased.</param>
        public AliasTypeAttribute( Type alias ) {
            Ensure.NotNull( alias );

            this.Alias = alias;
        }

    }

}
