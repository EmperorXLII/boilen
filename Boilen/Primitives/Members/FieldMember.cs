﻿
using System;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for a field declaration.
    /// </summary>
    public sealed class FieldMember : ModifiedTypedMember {

        /// <inheritdoc/>
        public override Extensions UsedExtensions { get { return Extensions.None; } }


        /// <summary>
        /// Initializes a new <see cref="FieldMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the field.</param>
        /// <param name="type">The type of the field.</param>
        public FieldMember( string name, string type )
            : base( name, type, "private" ) {
        }


        /// <inheritdoc/>
        protected override void WriteCore( ICodeWriter writer ) {
            writer.WriteLine( "{0} {1} {2};", this.Modifiers, this.Type, this.Name );
        }

    }

}
