﻿
using System;


namespace Boilen {

    /// <summary>
    /// Specifies the available property value change callbacks.
    /// </summary>
    public enum Changed {

        None = 0,
        Static,
        Virtual,
        Instance,
        Parameterless,

    }

}
