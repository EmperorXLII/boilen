
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        /// <include file='prefix/SourceStruct.xml' path='/doc/member[@name="IntProperty"]/*' />
        public int IntProperty
        {
            get { return 3; }
        }

    }

}
