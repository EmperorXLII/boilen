
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private readonly int _privateBackingFieldForProperty_Name;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class with the specified name.
        /// </summary>
        /// <param name='name'>
        /// Description.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Category", "ID", Justification = "Justification")]
        internal SourceClass(int name)
        {
            this._privateBackingFieldForProperty_Name = name;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets description.
        /// </summary>
        public int Name
        {
            get { return this._privateBackingFieldForProperty_Name; }
        }

    }

}
