
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.State'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.State'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StateProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.StateProperty = DependencyProperty.Register(
                "State", typeof(FreezableImpl), typeof(SourceFreezable),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceFreezable.StateChangedShim)
                )
            );
#else
            SourceFreezable.StateProperty = DependencyProperty.Register(
                "State", typeof(FreezableImpl), typeof(SourceFreezable),
                new PropertyMetadata(
                    new FreezableImpl()
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public FreezableImpl State
        {
            get { return (FreezableImpl)this.GetValue(SourceFreezable.StateProperty); }
            set { this.SetValue(SourceFreezable.StateProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL;

        private static void StateChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.StateProperty, e.OldValue);
                self._privateBackingFieldForProperty_State_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            e.OldValue.SafeSubpropertyChanged(self.StateSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.StateSubpropertyChanged_SL, true);

            self.OnSubpropertyChanged();
        }

        private void StateSubpropertyChanged_SL(object sender, EventArgs e)
        {
            this.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
            this.State.SafeFreeze();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
