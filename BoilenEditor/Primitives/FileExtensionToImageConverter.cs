﻿
using System;
using System.Globalization;
using System.Windows.Data;


namespace BoilenEditor.Primitives {

    public sealed class FileExtensionToImageConverter : IValueConverter {

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) {
            string extension = (string)value;
            string imagePath = "Images/" + extension + ".png";
            return imagePath;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) {
            throw new NotSupportedException( );
        }

    }

}
