﻿
using Boilen.Primitives.Members;
using System.Collections.Generic;
using System.Linq;


namespace Boilen.Primitives.Implementers {

    /// <inheritdoc/>
    /// <summary>
    /// Implements an immutable property that returns a constant value.
    /// </summary>
    public sealed class ConstantProperty<T> : ValueAccessor<T> {

        internal const string DescriptionFormat = "Gets {0}.";


        /// <inheritdoc/>
        protected override IEnumerable<FieldMember> Fields {
            get { return Enumerable.Empty<FieldMember>( ); }
        }

        /// <inheritdoc/>
        protected override IEnumerable<InitializationMember> Initializers {
            get { return Enumerable.Empty<InitializationMember>( ); }
        }

        /// <inheritdoc/>
        protected override AccessorMember Accessor {
            get {
                string defaultValue = this.DefaultValues.Values.Single( );
                return new AccessorMember( this.AccessorName, this.TypeName ) {
                    Doc = this.CreateDoc( )
                        .AddSummary( DescriptionFormat, this.Description ),
                    Modifiers = this.AccessorModifiers,
                    ObserveMember = this.CreateAccessorBlock(
                        ObserveAccessorName,
                        string.Format( "return {0};", defaultValue )
                    )
                };
            }
        }


        /// <inheritdoc/>
        public ConstantProperty( PartialType parent, string name, string description, T value )
            : base( parent, name, description ) {
            this.SetDefaultValue( value );
        }

    }

}
