
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

#if SL3_0
        private static readonly DependencyProperty StatePropertyKey;

        internal static readonly DependencyProperty StateProperty;
#else
        private static readonly DependencyPropertyKey StatePropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.State'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.State'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty StateProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.StatePropertyKey = DependencyProperty.Register(
                "State", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int)
                )
            );
            SourceGenericDependencyObject<T>.StateProperty = StatePropertyKey;
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.StatePropertyKey = DependencyProperty.RegisterReadOnly(
                "State", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int)
                )
            );
            SourceGenericDependencyObject<T>.StateProperty = StatePropertyKey.DependencyProperty;
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceGenericDependencyObject{T}'/> class.
        /// </summary>
        public SourceGenericDependencyObject()
        {
            this.State = new int();

            this.InitializeInstance();
        }

        static partial void InitializeType();

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-state.
        /// </summary>
        public int State
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.StateProperty); }
            protected set { this.SetValue(SourceGenericDependencyObject<T>.StatePropertyKey, value); }
        }

    }

}
