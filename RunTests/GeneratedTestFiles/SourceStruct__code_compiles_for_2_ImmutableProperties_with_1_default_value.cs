
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;
        private readonly string _privateBackingFieldForProperty_StringProperty;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified stringProperty and intProperty.
        /// </summary>
        /// <param name='stringProperty'>
        /// Test-description-for-stringProperty.
        /// </param>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public SourceStruct(string stringProperty, int intProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;
            this._privateBackingFieldForProperty_StringProperty = stringProperty;

            this.InitializeInstance();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified stringProperty.
        /// </summary>
        /// <param name='stringProperty'>
        /// Test-description-for-stringProperty.
        /// </param>
        public SourceStruct(string stringProperty)
            : this(stringProperty, 0) { }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

        /// <summary>
        /// Gets test-description-for-stringProperty.
        /// </summary>
        public string StringProperty
        {
            get { return this._privateBackingFieldForProperty_StringProperty; }
        }

    }

}
