﻿
// <auto-generated>
//   !!! WARNING !!!
//   !!!   This code was automatically generated from a T4 template.
//   !!!   Do not edit this file directly. Changes to this file will be lost.
//   !!! WARNING !!!
//   
//      Source Template - OpenData.b.tt
// </auto-generated>


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using BoilenEditor.Restricted;


namespace BoilenEditor.Primitives
{

    partial class OpenData :
        INotifyPropertyChanged
    {

        private string _privateBackingFieldForProperty_SolutionPath;
        private string _privateBackingFieldForProperty_SolutionDirectory;
        private string _privateBackingFieldForProperty_ProjectDirectoryFilter;
        private bool _privateBackingFieldForProperty_IncludeEmptyProjects;
        private readonly ObservableCollection<ProjectData> _privateBackingFieldForProperty_Projects;


        /// <summary>
        /// Initializes a new instance of the <see cref='OpenData'/> class.
        /// </summary>
        public OpenData()
        {
            this._privateBackingFieldForProperty_Projects = new ObservableCollection<ProjectData>( );

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets or sets the path to the solution file where the projects are located.
        /// </summary>
        public string SolutionPath
        {
            get { return this._privateBackingFieldForProperty_SolutionPath; }
            set
            {
                this.SolutionPathCoerce(ref value);

                if (!EqualityComparer<string>.Default.Equals(this._privateBackingFieldForProperty_SolutionPath, value))
                {
                    this.SolutionPathChanging(value);
                    string oldValue = this._privateBackingFieldForProperty_SolutionPath;
                    this._privateBackingFieldForProperty_SolutionPath = value;
                    this.SolutionPathChanged(oldValue);
                    this.OnPropertyChanged(SolutionPathPropertyName);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void SolutionPathCoerce(ref string value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void SolutionPathChanging(string newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void SolutionPathChanged(string oldValue);

        /// <summary>
        /// Gets or sets the directory where the projects are located.
        /// </summary>
        public string SolutionDirectory
        {
            get { return this._privateBackingFieldForProperty_SolutionDirectory; }
            set
            {
                this.SolutionDirectoryCoerce(ref value);

                if (!EqualityComparer<string>.Default.Equals(this._privateBackingFieldForProperty_SolutionDirectory, value))
                {
                    this.SolutionDirectoryChanging(value);
                    string oldValue = this._privateBackingFieldForProperty_SolutionDirectory;
                    this._privateBackingFieldForProperty_SolutionDirectory = value;
                    this.SolutionDirectoryChanged(oldValue);
                    this.OnPropertyChanged(SolutionDirectoryPropertyName);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void SolutionDirectoryCoerce(ref string value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void SolutionDirectoryChanging(string newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void SolutionDirectoryChanged(string oldValue);

        /// <summary>
        /// Gets or sets the filter to apply to project directories.
        /// </summary>
        public string ProjectDirectoryFilter
        {
            get { return this._privateBackingFieldForProperty_ProjectDirectoryFilter; }
            set
            {
                this.ProjectDirectoryFilterCoerce(ref value);

                if (!EqualityComparer<string>.Default.Equals(this._privateBackingFieldForProperty_ProjectDirectoryFilter, value))
                {
                    this.ProjectDirectoryFilterChanging(value);
                    string oldValue = this._privateBackingFieldForProperty_ProjectDirectoryFilter;
                    this._privateBackingFieldForProperty_ProjectDirectoryFilter = value;
                    this.ProjectDirectoryFilterChanged(oldValue);
                    this.OnPropertyChanged(ProjectDirectoryFilterPropertyName);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void ProjectDirectoryFilterCoerce(ref string value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void ProjectDirectoryFilterChanging(string newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void ProjectDirectoryFilterChanged(string oldValue);

        /// <summary>
        /// Gets or sets a value indicating whether projects without template files should be included.
        /// </summary>
        public bool IncludeEmptyProjects
        {
            get { return this._privateBackingFieldForProperty_IncludeEmptyProjects; }
            set
            {
                this.IncludeEmptyProjectsCoerce(ref value);

                if (!EqualityComparer<bool>.Default.Equals(this._privateBackingFieldForProperty_IncludeEmptyProjects, value))
                {
                    this.IncludeEmptyProjectsChanging(value);
                    bool oldValue = this._privateBackingFieldForProperty_IncludeEmptyProjects;
                    this._privateBackingFieldForProperty_IncludeEmptyProjects = value;
                    this.IncludeEmptyProjectsChanged(oldValue);
                    this.OnPropertyChanged(IncludeEmptyProjectsPropertyName);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IncludeEmptyProjectsCoerce(ref bool value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IncludeEmptyProjectsChanging(bool newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IncludeEmptyProjectsChanged(bool oldValue);

        /// <summary>
        /// Gets the collection of C# projects under the solution directory.
        /// </summary>
        public ObservableCollection<ProjectData> Projects
        {
            get { return this._privateBackingFieldForProperty_Projects; }
        }


        #region INotifyPropertyChanged Members

        private PropertyChangedEventHandler _privateBackingFieldForEvent_PropertyChanged;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add { this._privateBackingFieldForEvent_PropertyChanged += value; }
            remove { this._privateBackingFieldForEvent_PropertyChanged -= value; }
        }

        /// <summary>
        /// Raises the <see cref='OpenData.PropertyChanged'/> event.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='e'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='e'>
        /// An instance of <see cref='PropertyChangedEventArgs'/> that contains the event data.
        /// </param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            e.GuardParam("e")
                .NotNull();

            PropertyChangedEventHandler handler = Interlocked.CompareExchange(ref this._privateBackingFieldForEvent_PropertyChanged, null, null);
            if (!object.ReferenceEquals(handler, null))
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref='OpenData.PropertyChanged'/> event.
        /// </summary>
        /// <param name='propertyName'>
        /// The name of the property that changed.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Justification = "Supports the PropertyChanged event.")]
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
            this.OnPropertyChanged(e);
        }

        #endregion
        #region Property Names

        /// <summary>
        /// The name of the <see cref='OpenData.SolutionPath'/> property.
        /// </summary>
        public const string SolutionPathPropertyName = "SolutionPath";

        /// <summary>
        /// The name of the <see cref='OpenData.SolutionDirectory'/> property.
        /// </summary>
        public const string SolutionDirectoryPropertyName = "SolutionDirectory";

        /// <summary>
        /// The name of the <see cref='OpenData.ProjectDirectoryFilter'/> property.
        /// </summary>
        public const string ProjectDirectoryFilterPropertyName = "ProjectDirectoryFilter";

        /// <summary>
        /// The name of the <see cref='OpenData.IncludeEmptyProjects'/> property.
        /// </summary>
        public const string IncludeEmptyProjectsPropertyName = "IncludeEmptyProjects";

        #endregion

    }

}
