
using System;
using System.Windows;
using System.Windows.Media;
using Xunit;


namespace Boilen {

    public class TestPartial {

        [Fact]
        public void Type_method_Type_fails_for_null_arguments( ) {
            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( NullType ) );
        }

        [Fact]
        public void Type_method_Name_Type_fails_for_null_arguments( ) {
            string name = "name";
            Type type = typeof( object );

            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( NullName, type ) );
            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( name, NullType ) );
        }

        [Fact]
        public void Type_method_Type_BaseType_fails_for_null_arguments( ) {
            Type type = typeof( object );
            Type baseType = typeof( object );

            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( NullType, baseType ) );
            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( type, NullType ) );
        }

        [Fact]
        public void Type_method_Type_BaseType_fails_for_same_Type( ) {
            Type type = typeof( object );
            Type baseType = typeof( object );

            Assert.Throws<ArgumentException>( ( ) => Partial.Type( type, baseType ) );
        }

        [Fact]
        public void Type_method_Type_BaseType_SilverlightBaseTypeName_fails_for_null_arguments( ) {
            Type type = typeof( Brush );
            Type baseType = typeof( Freezable );
            string silverlightBaseTypeName = typeof( FrameworkElement ).FullName;

            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( NullType, baseType, silverlightBaseTypeName ) );
            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( type, NullType, silverlightBaseTypeName ) );
            Assert.Throws<ArgumentNullException>( ( ) => Partial.Type( type, baseType, NullName ) );
        }

        [Fact]
        public void Type_method_Type_BaseType_SilverlightBaseTypeName_fails_for_same_Type( ) {
            Type type = typeof( object );
            Type baseType = typeof( object );
            string silverlightBaseTypeName = typeof( FrameworkElement ).FullName;

            Assert.Throws<ArgumentException>( ( ) => Partial.Type( type, baseType, silverlightBaseTypeName ) );
        }


        [Fact]
        public void Type_succeeds_for_valid_type( ) {
            Type type = typeof( string );

            var partialType = Partial.Type( type );

            Assert.Equal( partialType.Type, type );
        }

        [Fact]
        public void Type_T_succeeds_for_valid_type( ) {
            Type expectedType = typeof( string );

            var partialType = Partial.Type<string>( );

            Assert.Equal( partialType.Type, expectedType );
        }


        #region Utility

        private const Type NullType = null;
        private const string NullName = null;

        #endregion

    }

}
