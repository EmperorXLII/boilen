﻿
using System.Diagnostics;
using System.IO;


namespace BoilenEditor.Primitives {

    [DebuggerDisplay( "FileData: FilePath={FilePath}" )]
    public partial class FileData {

        /// <summary>Gets a value indicating whether the template file is a Boilen template file.</summary>
        public bool IsBoilenFile {
            get { return this.FilePath.EndsWith( ".b.tt" ); }
        }

        /// <summary>Gets a value indicating whether the template file is a reference-only template file.</summary>
        public bool IsReferenceFile {
            get { return !Path.GetFileNameWithoutExtension( this.Name ).Contains( "." ); }
        }


        protected virtual string GetDisplayName( string path, string rootDirectory ) {
            string displayName = Path.GetFileName( path ).Replace( ".b.tt", "" );
            if( displayName.Contains( "`" ) )
                displayName = displayName.Replace( '`', '<' ) + ">";
            return displayName;
        }

        private static string GetGroup( string path, string rootDirectory ) {
            string group = "";
            string subpath = path;
            while( !string.IsNullOrWhiteSpace( subpath ) ) {
                subpath = Path.GetDirectoryName( subpath );
                string part = Path.GetFileName( subpath );
                group = part + (group.Length == 0 ? group : Path.DirectorySeparatorChar + group);

                if( subpath == rootDirectory || part == "Source" )
                    break;
            }

            return group;
        }

        private string GetSearchString( ) {
            string group = this.Group;
            string name = this.DisplayName;

            if( group.Length == 0 )
                return name;

            string altGroup = group.Replace( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar );
            return group + Path.DirectorySeparatorChar + name
                + "$"
                + altGroup + Path.AltDirectorySeparatorChar + name;
        }

    }

}
