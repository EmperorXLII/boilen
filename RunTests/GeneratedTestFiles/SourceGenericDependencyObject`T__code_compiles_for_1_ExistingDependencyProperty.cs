
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.Fill" /> dependency property. This field is read-only.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.Fill" /> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty FillProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.FillProperty = DependencyProperty.Register(
                "Fill", typeof(Brush), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    (Brush)null
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.FillProperty = Shape.FillProperty.AddOwner(typeof(SourceGenericDependencyObject<T>));
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets the <see cref="System.Windows.Media.Brush" /> that specifies how the test-documentation-update's interior is painted.
        /// </summary>
        /// <returns>
        /// A <see cref="System.Windows.Media.Brush" /> that describes how the test-documentation-update's interior is painted. The default is <see langword='null'/>.
        /// </returns>
        public Brush Fill
        {
            get { return (Brush)this.GetValue(SourceGenericDependencyObject<T>.FillProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.FillProperty, value); }
        }

    }

}
