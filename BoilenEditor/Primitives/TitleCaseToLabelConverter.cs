﻿
using System;
using System.Globalization;
using System.Text;
using System.Windows.Data;


namespace BoilenEditor.Primitives {

    public sealed class TitleCaseToLabelConverter : IValueConverter {

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) {
            var sb = new StringBuilder( value.ToString( ) );

            int capitalLetterIndex = 1;
            while( FindCapitalLetter( sb, ref capitalLetterIndex ) ) {
                sb.Insert( capitalLetterIndex, ' ' );
                capitalLetterIndex += 2;
            }

            return sb.ToString( );
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) {
            throw new NotSupportedException( );
        }


        private static bool FindCapitalLetter( StringBuilder sb, ref int index ) {
            for( int i = index; i < sb.Length; ++i )
                if( char.IsUpper( sb[i] ) ) {
                    index = i;
                    return true;
                }

            return false;
        }

    }

}
