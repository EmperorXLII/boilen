﻿
// <auto-generated>
//   !!! WARNING !!!
//   !!!   This code was automatically generated from a T4 template.
//   !!!   Do not edit this file directly. Changes to this file will be lost.
//   !!! WARNING !!!
//   
//      Source Template - SettingsData.b.tt
// </auto-generated>


using System;
using System.Collections.ObjectModel;
using BoilenEditor.Properties;


namespace BoilenEditor.Primitives
{

    partial class SettingsData
    {

        private readonly Settings _privateBackingFieldForProperty_Settings;
        private readonly ReadOnlyCollection<SettingPropertyData> _privateBackingFieldForProperty_Properties;


        /// <summary>
        /// Initializes a new instance of the <see cref='SettingsData'/> class with the specified settings.
        /// </summary>
        /// <param name='settings'>
        /// The application settings object.
        /// </param>
        internal SettingsData(Settings settings)
        {
            this._privateBackingFieldForProperty_Settings = settings;

            this._privateBackingFieldForProperty_Properties = GetProperties( settings );

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets the application settings object.
        /// </summary>
        internal Settings Settings
        {
            get { return this._privateBackingFieldForProperty_Settings; }
        }

        /// <summary>
        /// Gets the properties in the <see cref='Settings'/> object.
        /// </summary>
        public ReadOnlyCollection<SettingPropertyData> Properties
        {
            get { return this._privateBackingFieldForProperty_Properties; }
        }

    }

}
