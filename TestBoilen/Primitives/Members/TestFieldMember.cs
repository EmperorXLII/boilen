
using Xunit.Extensions;


namespace Boilen.Primitives.Members {

    public class TestFieldMember {

        [Theory]
        [InlineData( "modifiers", "type", "name" )]
        [InlineData( "public", "int", "count" )]
        public void Write_succeeds_for_valid_arguments( string modifiers, string type, string name ) {
            string[] expectedLines = new[] { modifiers + " " + type + " " + name + ";", "" };
            var writer = new StringBuilderCodeWriter( );
            var fm = new FieldMember( name, type ) { Modifiers = modifiers };

            fm.Write( writer );

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }

    }

}
