
using System;
using System.Windows;
using Boilen.Freezables;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StateProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.StateProperty = DependencyProperty.Register(
                "State", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceDependencyObject.StateChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.StateProperty = DependencyProperty.Register(
                "State", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    new FreezableImpl()
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public FreezableImpl State
        {
            get { return (FreezableImpl)this.GetValue(SourceDependencyObject.StateProperty); }
            set { this.SetValue(SourceDependencyObject.StateProperty, value); }
        }

#if SL3_0
        private static void StateChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            object localValue = self.ReadLocalValue(StateProperty);
            if (object.ReferenceEquals(localValue, DependencyProperty.UnsetValue))
                e.NewValue.SafeFreeze();
        }
#endif

    }

}
