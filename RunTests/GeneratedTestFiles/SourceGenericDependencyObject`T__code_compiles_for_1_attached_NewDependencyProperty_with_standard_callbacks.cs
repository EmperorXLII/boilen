
using System;
using System.Windows;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.StandardCallbacks'/> attached property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.StandardCallbacks'/> attached property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty StandardCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.StandardCallbacksProperty = DependencyProperty.RegisterAttached(
                "StandardCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.StandardCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.StandardCallbacksProperty = DependencyProperty.RegisterAttached(
                "StandardCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new FrameworkPropertyMetadata(
                    default(int),
                    FrameworkPropertyMetadataOptions.AffectsArrange,
                    (PropertyChangedCallback)null,
                    new CoerceValueCallback(DependencyPropertyCallbacks.CoerceNonNegative<int>)
                ),
                new ValidateValueCallback(DependencyPropertyCallbacks.IsValidEnum<int>)
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.StandardCallbacks" /> attached property for a specified <see cref="System.Windows.UIElement" />.
        /// </summary>
        /// <returns>
        /// The <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.StandardCallbacks" /> property value for the element.
        /// </returns>
        /// <remarks>
        /// Gets test-description-for-standardCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element from which the property value is read.
        /// </param>
        public static int GetStandardCallbacks(UIElement element)
        {
            element.GuardParam("element")
                .NotNull();

            return (int)element.GetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty);
        }

        /// <summary>
        /// Sets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.StandardCallbacks" /> attached property to a specified element.
        /// </summary>
        /// <remarks>
        /// Sets test-description-for-standardCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element to which the attached property is written.
        /// </param>
        /// <param name='value'>
        /// The needed <see cref="int" /> value.
        /// </param>
        public static void SetStandardCallbacks(UIElement element, int value)
        {
            element.GuardParam("element")
                .NotNull();

            element.SetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty, value);
        }
#if SL3_0
        private static void StandardCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            object value = e.NewValue;
            object coercedValue = DependencyPropertyCallbacks.CoerceNonNegative<int>(d, value);
            if (!object.Equals(coercedValue, value))
            {
                d.SetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = DependencyPropertyCallbacks.IsValidEnum<int>(value);
            if (!isValid)
            {
                d.SetValue(SourceGenericDependencyObject<T>.StandardCallbacksProperty, e.OldValue);
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'StandardCallbacks'.", value);
            }

            UIElement target = d as UIElement;
            if (!object.ReferenceEquals(target, null))
                target.InvalidateArrange();
        }
#endif

    }

}
