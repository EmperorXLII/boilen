
using System;
using System.Threading;
using System.Windows.Input;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private MouseEventHandler _privateBackingFieldForEvent_State;


        /// <summary>
        /// Occurs when test-description-for-state.
        /// </summary>
        internal event MouseEventHandler State
        {
            add { this._privateBackingFieldForEvent_State += value; }
            remove { this._privateBackingFieldForEvent_State -= value; }
        }

        /// <summary>
        /// Raises the <see cref='SourceClass.State'/> event.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='e'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='e'>
        /// An instance of <see cref='MouseEventArgs'/> that contains the event data.
        /// </param>
        internal virtual void OnState(MouseEventArgs e)
        {
            e.GuardParam("e")
                .NotNull();

            MouseEventHandler handler = Interlocked.CompareExchange(ref this._privateBackingFieldForEvent_State, null, null);
            if (!object.ReferenceEquals(handler, null))
            {
                handler(this, e);
            }
        }

    }

}
