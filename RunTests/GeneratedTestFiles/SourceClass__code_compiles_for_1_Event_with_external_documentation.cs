
using System;
using System.Threading;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private EventHandler _privateBackingFieldForEvent_State;


        /// <include file='prefix/SourceClass.xml' path='/doc/member[@name="State"]/*' />
        public event EventHandler State
        {
            add { this._privateBackingFieldForEvent_State += value; }
            remove { this._privateBackingFieldForEvent_State -= value; }
        }

        /// <summary>
        /// Raises the <see cref='SourceClass.State'/> event.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='e'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='e'>
        /// An instance of <see cref='EventArgs'/> that contains the event data.
        /// </param>
        protected virtual void OnState(EventArgs e)
        {
            e.GuardParam("e")
                .NotNull();

            EventHandler handler = Interlocked.CompareExchange(ref this._privateBackingFieldForEvent_State, null, null);
            if (!object.ReferenceEquals(handler, null))
            {
                handler(this, e);
            }
        }

    }

}
