﻿
using System;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for a member with a type.
    /// </summary>
    public abstract class TypedMember : Member {

        private readonly string type_;


        /// <summary>
        /// Gets the type of the member.
        /// </summary>
        public string Type { get { return this.type_; } }


        /// <summary>
        /// Initializes a new <see cref="TypedMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the member.</param>
        /// <param name="type">The type of the member.</param>
        /// <param name="isTypeRequired">Indicates whether or not <paramref name="type"/> is required.</param>
        protected TypedMember( string name, string type, bool isTypeRequired )
            : base( name ) {
            if( isTypeRequired )
                Ensure.NotNullOrEmpty( type );

            this.type_ = type ?? "";
        }

    }

}
