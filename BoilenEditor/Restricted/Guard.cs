﻿
using System;


namespace BoilenEditor.Restricted {

    internal struct Guard<T> {

        public readonly T Value;
        public readonly string Description;

        public Guard( T value, string description ) {
            this.Value = value;
            this.Description = description;
        }

    }

}
