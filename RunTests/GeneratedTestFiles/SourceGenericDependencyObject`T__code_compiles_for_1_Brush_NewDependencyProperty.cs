
using System;
using System.Windows;
using System.Windows.Media;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Brush'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Brush'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty BrushProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.BrushProperty = DependencyProperty.Register(
                "Brush", typeof(Brush), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    new SolidColorBrush(Colors.Red)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.BrushProperty = DependencyProperty.Register(
                "Brush", typeof(Brush), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    Brushes.Red
                )
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-brush.
        /// </summary>
        public Brush Brush
        {
            get { return (Brush)this.GetValue(SourceGenericDependencyObject<T>.BrushProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.BrushProperty, value); }
        }

    }

}
