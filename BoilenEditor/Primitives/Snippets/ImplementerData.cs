﻿
using Boilen.Primitives;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;


namespace BoilenEditor.Primitives.Snippets {

    public partial class ImplementerData {

        private static ReadOnlyCollection<DeclarationData> GetDeclarations( ) {
            var declarations =
                from method in typeof( PartialType ).GetMethods( BindingFlags.Public | BindingFlags.Instance )
                let category = method.GetCustomAttribute<CategoryAttribute>( )
                let displayName = method.GetCustomAttribute<DisplayNameAttribute>( )
                where category.HasValue( )
                   && displayName.HasValue( )
                orderby displayName.DisplayName
                select new DeclarationData( method, category.Category, displayName.DisplayName );

            return declarations.ToReadOnlyCollection( );
        }

    }

}
