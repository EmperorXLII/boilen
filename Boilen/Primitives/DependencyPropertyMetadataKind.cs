﻿
using System.Windows;


namespace Boilen.Primitives {

    /// <summary>
    /// Represents the types of dependency property metadata; 
    /// either <see cref="PropertyMetadata"/>, <see cref="UIPropertyMetadata"/>, or <see cref="FrameworkPropertyMetadata"/>.
    /// </summary>
    public enum DependencyPropertyMetadataKind {

        Base = 0,
        UI,
        Framework,

    }

}
