
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.None'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.None'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty NoneProperty;
#if CUSTOM
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Custom'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Custom'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty CustomProperty;
#endif
#if SL3_0
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Silverlight'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Silverlight'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty SilverlightProperty;
#else
        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.NotSilverlight'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.NotSilverlight'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty NotSilverlightProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
            SourceGenericDependencyObject<T>.NoneProperty = DependencyProperty.Register(
                "None", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int)
                )
            );
#if CUSTOM
            SourceGenericDependencyObject<T>.CustomProperty = DependencyProperty.Register(
                "Custom", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int)
                )
            );
#endif
#if SL3_0
            SourceGenericDependencyObject<T>.SilverlightProperty = DependencyProperty.Register(
                "Silverlight", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.NotSilverlightProperty = DependencyProperty.Register(
                "NotSilverlight", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int)
                )
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int None
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.NoneProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.NoneProperty, value); }
        }
#if CUSTOM
        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int Custom
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.CustomProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.CustomProperty, value); }
        }
#endif
#if SL3_0
        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int Silverlight
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.SilverlightProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.SilverlightProperty, value); }
        }
#else
        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
        public int NotSilverlight
        {
            get { return (int)this.GetValue(SourceGenericDependencyObject<T>.NotSilverlightProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.NotSilverlightProperty, value); }
        }
#endif

    }

}
