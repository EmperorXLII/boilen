﻿
using System;


namespace Boilen {

    /// <summary>
    /// Specifies the pre-defined kinds of property value validation.
    /// </summary>
    public enum Validate {

        None = 0,
        Custom,
        Enum,
        NotNull,

    }

}
