
using System;
using System.Windows;
using Boilen.Freezables;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.VirtualCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.VirtualCallbacks'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty VirtualCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(FreezableImpl), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.VirtualCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(FreezableImpl), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    new FreezableImpl(),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.VirtualCallbacksChangedShim)
                )
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-virtualCallbacks.
        /// </summary>
        public FreezableImpl VirtualCallbacks
        {
            get { return (FreezableImpl)this.GetValue(SourceGenericDependencyObject<T>.VirtualCallbacksProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.VirtualCallbacksProperty, value); }
        }

        /// <summary>
        /// Called when the <see cref="SourceGenericDependencyObject{T}.VirtualCallbacks" /> property changes.
        /// </summary>
        /// <param name='oldValue'>
        /// Old value of the <see cref="SourceGenericDependencyObject{T}.VirtualCallbacks" /> property.
        /// </param>
        /// <param name='newValue'>
        /// New value of the <see cref="SourceGenericDependencyObject{T}.VirtualCallbacks" /> property.
        /// </param>
        protected virtual void OnVirtualCallbacksChanged(FreezableImpl oldValue, FreezableImpl newValue)
        {
            this.OnVirtualCallbacksChanged();
        }

        partial void OnVirtualCallbacksChanged();
#if SL3_0
        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            object localValue = self.ReadLocalValue(VirtualCallbacksProperty);
            if (object.ReferenceEquals(localValue, DependencyProperty.UnsetValue))
                e.NewValue.SafeFreeze();

            e.OldValue.SafeSubpropertyChanged(self.VirtualCallbacksSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.VirtualCallbacksSubpropertyChanged_SL, true);

            self.OnVirtualCallbacksChanged((FreezableImpl)e.OldValue, (FreezableImpl)e.NewValue);
        }

        private void VirtualCallbacksSubpropertyChanged_SL(object sender, EventArgs e)
        {
            FreezableImpl value = this.VirtualCallbacks;
            this.OnVirtualCallbacksChanged(value, value);
        }
#else
        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            self.OnVirtualCallbacksChanged((FreezableImpl)e.OldValue, (FreezableImpl)e.NewValue);
        }
#endif

    }

}
