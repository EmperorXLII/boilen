
using System;
using Xunit;


namespace Boilen.Primitives.Members {

    public class TestTypedMember {

        [Fact]
        public void constructor_fails_for_null_type_when_required( ) {
            string name = "name";
            string nullType = null;
            bool isTypeRequired = true;

            Assert.Throws<ArgumentNullException>( ( ) => new TypedMemberCtor( name, nullType, isTypeRequired ) );
        }

        [Fact]
        public void constructor_fails_for_empty_type_when_required( ) {
            string name = "name";
            string emptyType = "";
            bool isTypeRequired = true;

            Assert.Throws<ArgumentException>( ( ) => new TypedMemberCtor( name, emptyType, isTypeRequired ) );
        }

        [Fact]
        public void constructor_succeeds_for_valid_arguments( ) {
            string name = "name";
            string type = "type";
            bool isTypeRequired = true;

            var tm = new TypedMemberCtor( name, type, isTypeRequired );

            Assert.Equal( tm.Name, name );
            Assert.Equal( tm.Type, type );
        }

        [Fact]
        public void constructor_succeeds_for_null_type_when_optional( ) {
            string name = "name";
            string nullType = null;
            bool isTypeRequired = false;

            var tm = new TypedMemberCtor( name, nullType, isTypeRequired );

            Assert.Equal( tm.Name, name );
            Assert.Equal( tm.Type, "" );
        }

        [Fact]
        public void constructor_succeeds_for_empty_type_when_optional( ) {
            string name = "name";
            string emptyType = "";
            bool isTypeRequired = false;

            var tm = new TypedMemberCtor( name, emptyType, isTypeRequired );

            Assert.Equal( tm.Name, name );
            Assert.Equal( tm.Type, "" );
        }


        #region Utility

        private sealed class TypedMemberCtor : TypedMember {
            public TypedMemberCtor( string name, string type, bool isTypeRequired ) : base( name, type, isTypeRequired ) { }
            public override Extensions UsedExtensions { get { return Extensions.None; } }
            protected override void WriteCore( ICodeWriter writer ) { }
        }

        #endregion

    }

}
