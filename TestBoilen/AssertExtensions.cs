﻿
using System.Collections.Generic;
using System.Linq;
using Xunit;


namespace Boilen {

    public static class AssertExtensions {

        public static void AddRange<T>( this IList<T> list, IEnumerable<T> items ) {
            foreach( T item in items )
                list.Add( item );
        }


        public static void StartsWith( this string actual, string expected ) {
            Assert.True( actual.StartsWith( expected ), string.Format( "String '{0}' did not start with expected value '{1}'.", actual, expected ) );
        }

        public static void EqualCollection<T>( this IEnumerable<T> actual, IEnumerable<T> expected ) {
            T[] actualValues = actual.ToArray( );
            T[] expectedValues = expected.ToArray( );
            Assert.Equal( expectedValues, actualValues );
        }

    }

}
