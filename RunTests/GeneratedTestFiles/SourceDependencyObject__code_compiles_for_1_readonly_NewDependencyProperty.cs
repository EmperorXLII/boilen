
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

#if SL3_0
        private static readonly DependencyProperty StatePropertyKey;

        internal static readonly DependencyProperty StateProperty;
#else
        private static readonly DependencyPropertyKey StatePropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StateProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.StatePropertyKey = DependencyProperty.Register(
                "State", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
            SourceDependencyObject.StateProperty = StatePropertyKey;
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.StatePropertyKey = DependencyProperty.RegisterReadOnly(
                "State", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
            SourceDependencyObject.StateProperty = StatePropertyKey.DependencyProperty;
#endif

            SourceDependencyObject.InitializeType();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceDependencyObject'/> class.
        /// </summary>
        public SourceDependencyObject()
        {
            this.State = new int();

            this.InitializeInstance();
        }

        static partial void InitializeType();

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-state.
        /// </summary>
        public int State
        {
            get { return (int)this.GetValue(SourceDependencyObject.StateProperty); }
            protected set { this.SetValue(SourceDependencyObject.StatePropertyKey, value); }
        }

    }

}
