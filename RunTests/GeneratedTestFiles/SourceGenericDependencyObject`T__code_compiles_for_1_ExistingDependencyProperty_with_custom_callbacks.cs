
using System;
using System.Windows;
using System.Windows.Shapes;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.StrokeDashOffset" /> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.StrokeDashOffset" /> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty StrokeDashOffsetProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.StrokeDashOffsetProperty = DependencyProperty.Register(
                "StrokeDashOffset", typeof(double), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    (double)0,
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.StrokeDashOffsetChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.StrokeDashOffsetProperty = Shape.StrokeDashOffsetProperty.AddOwner(
                typeof(SourceGenericDependencyObject<T>),
                new UIPropertyMetadata(
                    (double)0,
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.CustomChangedHandler),
                    new CoerceValueCallback(SourceGenericDependencyObject<T>.CustomCoerceHandler)
                )
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets a <see cref="System.Double" /> that specifies the distance within the dash pattern where a dash begins.
        /// </summary>
        /// <returns>
        /// A <see cref="System.Double" /> that represents the distance within the dash pattern where a dash begins.
        /// </returns>
        public double StrokeDashOffset
        {
            get { return (double)this.GetValue(SourceGenericDependencyObject<T>.StrokeDashOffsetProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.StrokeDashOffsetProperty, value); }
        }

#if SL3_0
        private static void StrokeDashOffsetChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            object value = e.NewValue;
            object coercedValue = SourceGenericDependencyObject<T>.CustomCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                self.SetValue(SourceGenericDependencyObject<T>.StrokeDashOffsetProperty, coercedValue);
                return;
            }

            SourceGenericDependencyObject<T>.CustomChangedHandler(self, e);
        }
#endif

    }

}
