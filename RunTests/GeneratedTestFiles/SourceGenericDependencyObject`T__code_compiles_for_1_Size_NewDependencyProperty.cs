
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Size'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Size'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty SizeProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
            SourceGenericDependencyObject<T>.SizeProperty = DependencyProperty.Register(
                "Size", typeof(Size), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    new Size(5, double.NaN)
                )
            );
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-size.
        /// </summary>
        public Size Size
        {
            get { return (Size)this.GetValue(SourceGenericDependencyObject<T>.SizeProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.SizeProperty, value); }
        }

    }

}
