﻿
using System;


namespace Boilen {

    /// <summary>
    /// Marks a type defined in a template file.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface )]
    public sealed class TemplateTypeAttribute : Attribute {

        /// <summary>
        /// The namespace of the attributed type.
        /// </summary>
        public string Namespace { get; set; }


        /// <summary>
        /// Initializes a new <see cref="TemplateTypeAttribute"/> instance.
        /// </summary>
        public TemplateTypeAttribute( )
            : this( "" ) { }

        /// <summary>
        /// Initializes a new <see cref="TemplateTypeAttribute"/> instance.
        /// </summary>
        /// <param name="nmspace">The namespace of the template type.</param>
        public TemplateTypeAttribute( string nmspace ) {
            this.Namespace = nmspace ?? "";
        }

    }

}
