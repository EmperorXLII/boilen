﻿
using System;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for an inheritance declaration.
    /// </summary>
    public sealed class InheritanceMember : Member {

        private readonly Extensions extensions_;

        /// <inheritdoc/>
        public override Extensions UsedExtensions { get { return this.extensions_; } }


        /// <summary>
        /// Initializes a new <see cref="InheritanceMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the group.</param>
        /// <param name="extensions">Indicates whether the inherited type is in an extension namespace.</param>
        public InheritanceMember( string name, Extensions extensions )
            : base( name ) {
            this.extensions_ = extensions;
        }

        /// <inheritdoc/>
        public InheritanceMember( string name )
            : this( name, Extensions.None ) { }


        /// <inheritdoc/>
        protected override void WriteCore( ICodeWriter writer ) {
            writer.Write( "    " + this.Name );
        }

    }

}
