﻿
using System.Collections.Generic;
using System.Linq;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for a member with helpers.
    /// </summary>
    public abstract class HelpedMember : ModifiedTypedMember {

        private readonly List<Member> helpers_ = new List<Member>( );


        /// <inheritdoc/>
        public override Extensions UsedExtensions {
            get { return Util.GetUsedExtensions( this.helpers_ ); }
        }

        /// <summary>
        /// Gets the collection of helpers used by the member.
        /// </summary>
        public IList<Member> Helpers { get { return this.helpers_; } }


        /// <summary>
        /// Initializes a new <see cref="HelpedMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the member.</param>
        /// <param name="type">The type of the member.</param>
        /// <param name="modifiers">The modifiers on the member.</param>
        protected HelpedMember( string name, string type, string modifiers )
            : this( name, type, true, modifiers ) { }

        /// <summary>
        /// Initializes a new <see cref="HelpedMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the member.</param>
        /// <param name="type">The type of the member.</param>
        /// <param name="isTypeRequired">Indicates whether <paramref name="type"/> must be set.</param>
        /// <param name="modifiers">The modifiers on the member.</param>
        protected HelpedMember( string name, string type, bool isTypeRequired, string modifiers )
            : base( name, type, isTypeRequired, modifiers ) {
            this.SpaceOutput = true;
        }


        /// <inheritdoc/>
        protected override void PostWrite( ICodeWriter writer ) {
            base.PostWrite( writer );
            if( this.Helpers.Any( ) ) {
                writer.WriteLine( );
                WriteMembers( writer, this.Helpers );
            }
        }

    }

}
