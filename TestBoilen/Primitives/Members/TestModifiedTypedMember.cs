
using Xunit;


namespace Boilen.Primitives.Members {

    public class TestModifiedTypedMember {

        [Fact]
        public void constructor_succeeds_for_null_modifiers( ) {
            string name = "name";
            string type = "type";
            string nullModifiers = null;
            string expectedModifiers = "";

            var mtm = new ModifiedTypedMemberCtor( name, type, nullModifiers );

            Assert.Equal( mtm.Name, name );
            Assert.Equal( mtm.Type, type );
            Assert.Equal( mtm.Modifiers, expectedModifiers );
        }

        [Fact]
        public void constructor_succeeds_for_valid_arguments( ) {
            string name = "name";
            string type = "type";
            string modifiers = "modifiers";

            var mtm = new ModifiedTypedMemberCtor( name, type, modifiers );

            Assert.Equal( mtm.Name, name );
            Assert.Equal( mtm.Type, type );
            Assert.Equal( mtm.Modifiers, modifiers );
        }


        #region Utility

        private sealed class ModifiedTypedMemberCtor : ModifiedTypedMember {
            public ModifiedTypedMemberCtor( string name, string type, string modifiers ) : base( name, type, modifiers ) { }
            public override Extensions UsedExtensions { get { return Extensions.None; } }
            protected override void WriteCore( ICodeWriter writer ) { }
        }

        #endregion

    }

}
