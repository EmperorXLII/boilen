
using Xunit.Extensions;


namespace Boilen.Primitives.Members {

    public class TestParameterMember {

        [Theory]
        [InlineData( "type", "name" )]
        [InlineData( "int", "count" )]
        public void Write_succeeds_for_valid_arguments( string type, string name ) {
            string[] expectedLines = new[] { type + " " + name };
            var writer = new StringBuilderCodeWriter( );
            var pm = new ParameterMember( name, type );

            pm.Write( writer );

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }

    }

}
