
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Vaule'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Vaule'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty VauleProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
            SourceDependencyObject.VauleProperty = DependencyProperty.Register(
                "Vaule", typeof(double), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    double.NaN
                )
            );
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-vaule.
        /// </summary>
        public double Vaule
        {
            get { return (double)this.GetValue(SourceDependencyObject.VauleProperty); }
            set { this.SetValue(SourceDependencyObject.VauleProperty, value); }
        }

    }

}
