
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class with the specified intProperty.
        /// </summary>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public SourceClass(int intProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;

            this.InitializeInstance();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class.
        /// </summary>
        public SourceClass()
            : this(1) { }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

    }

}
