﻿
using System;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Represents an object that can be written.
    /// </summary>
    public interface IWritable {

        /// <summary>
        /// Writes the object to the specified <see cref="IFileWriter"/>.
        /// </summary>
        void Write( ICodeWriter writer );

    }

}
