﻿
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;


namespace BoilenEditor.Primitives {

    public partial class InlineItemBehavior : DependencyObject {

        private static void InlineChanged( DependencyObject d, DependencyPropertyChangedEventArgs e ) {
            TextBlock textblock = (TextBlock)d;
            InlineCollection inlines = textblock.Inlines;
            Inline inline = GetInline( textblock );

            inlines.Clear( );
            inlines.Add( inline );
        }

    }

}
