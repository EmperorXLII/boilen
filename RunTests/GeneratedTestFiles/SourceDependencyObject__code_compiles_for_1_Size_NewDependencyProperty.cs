
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Size'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Size'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty SizeProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
            SourceDependencyObject.SizeProperty = DependencyProperty.Register(
                "Size", typeof(Size), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    new Size(5, double.NaN)
                )
            );
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-size.
        /// </summary>
        public Size Size
        {
            get { return (Size)this.GetValue(SourceDependencyObject.SizeProperty); }
            set { this.SetValue(SourceDependencyObject.SizeProperty, value); }
        }

    }

}
