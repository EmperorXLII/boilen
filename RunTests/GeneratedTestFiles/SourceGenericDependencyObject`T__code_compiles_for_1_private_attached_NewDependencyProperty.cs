
using System;
using System.Windows;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.State'/> attached property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.State'/> attached property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        private static readonly DependencyProperty StateProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
            SourceGenericDependencyObject<T>.StateProperty = DependencyProperty.RegisterAttached(
                "State", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int)
                )
            );
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.State" /> attached property for a specified <see cref="System.Windows.UIElement" />.
        /// </summary>
        /// <returns>
        /// The <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.State" /> property value for the element.
        /// </returns>
        /// <remarks>
        /// Gets test-description-for-state.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element from which the property value is read.
        /// </param>
        private static int GetState(UIElement element)
        {
            element.GuardParam("element")
                .NotNull();

            return (int)element.GetValue(SourceGenericDependencyObject<T>.StateProperty);
        }

        /// <summary>
        /// Sets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.State" /> attached property to a specified element.
        /// </summary>
        /// <remarks>
        /// Sets test-description-for-state.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element to which the attached property is written.
        /// </param>
        /// <param name='value'>
        /// The needed <see cref="int" /> value.
        /// </param>
        private static void SetState(UIElement element, int value)
        {
            element.GuardParam("element")
                .NotNull();

            element.SetValue(SourceGenericDependencyObject<T>.StateProperty, value);
        }

    }

}
