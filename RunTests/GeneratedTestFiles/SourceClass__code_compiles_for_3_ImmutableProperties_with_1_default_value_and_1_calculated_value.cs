
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private readonly int _privateBackingFieldForProperty_Required;
        private readonly int _privateBackingFieldForProperty_Optional;
        private readonly int _privateBackingFieldForProperty_Calculated;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class with the specified required and optional.
        /// </summary>
        /// <param name='required'>
        /// Test-description-for-required.
        /// </param>
        /// <param name='optional'>
        /// Test-description-for-required.
        /// </param>
        public SourceClass(int required, int optional)
        {
            this._privateBackingFieldForProperty_Required = required;
            this._privateBackingFieldForProperty_Optional = optional;

            this._privateBackingFieldForProperty_Calculated = 3;

            this.InitializeInstance();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class with the specified required.
        /// </summary>
        /// <param name='required'>
        /// Test-description-for-required.
        /// </param>
        public SourceClass(int required)
            : this(required, 2) { }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-required.
        /// </summary>
        public int Required
        {
            get { return this._privateBackingFieldForProperty_Required; }
        }

        /// <summary>
        /// Gets test-description-for-required.
        /// </summary>
        public int Optional
        {
            get { return this._privateBackingFieldForProperty_Optional; }
        }

        /// <summary>
        /// Gets test-description-for-calculated.
        /// </summary>
        public int Calculated
        {
            get { return this._privateBackingFieldForProperty_Calculated; }
        }

    }

}
