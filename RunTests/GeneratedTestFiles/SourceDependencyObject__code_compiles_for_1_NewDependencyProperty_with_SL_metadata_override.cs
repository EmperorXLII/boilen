
using System;
using System.Windows;
using System.Windows.Media;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Override'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.Override'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty OverrideProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.OverrideProperty = DependencyProperty.Register(
                "Override", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.OverrideChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.OverrideProperty = DependencyProperty.Register(
                "Override", typeof(int), typeof(SourceDependencyObject),
                new FrameworkPropertyMetadata(
                    default(int),
                    FrameworkPropertyMetadataOptions.AffectsParentMeasure
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-override.
        /// </summary>
        public int Override
        {
            get { return (int)this.GetValue(SourceDependencyObject.OverrideProperty); }
            set { this.SetValue(SourceDependencyObject.OverrideProperty, value); }
        }

#if SL3_0
        private static void OverrideChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            CustomChangedHandler(self, e);
        }
#endif

    }

}
