
using System;
using System.Collections.Generic;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private int _privateBackingFieldForProperty_IntProperty;


        /// <summary>
        /// Gets or sets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
            set
            {
                this.IntPropertyCoerce(ref value);

                if (!EqualityComparer<int>.Default.Equals(this._privateBackingFieldForProperty_IntProperty, value))
                {
                    this.IntPropertyChanging(value);
                    int oldValue = this._privateBackingFieldForProperty_IntProperty;
                    this._privateBackingFieldForProperty_IntProperty = value;
                    this.IntPropertyChanged(oldValue);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IntPropertyCoerce(ref int value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IntPropertyChanging(int newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void IntPropertyChanged(int oldValue);

    }

}
