
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct<T>
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct{T}'/> struct with the specified intProperty.
        /// </summary>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public SourceStruct(int intProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

    }

}
