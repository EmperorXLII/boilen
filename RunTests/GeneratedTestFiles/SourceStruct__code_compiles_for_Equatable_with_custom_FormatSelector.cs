
using System;
using System.Collections.Generic;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct :
        IEquatable<SourceStruct>
    {

        private readonly string _privateBackingFieldForProperty_Value;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified value.
        /// </summary>
        /// <param name='value'>
        /// Test-description-for-value.
        /// </param>
        public SourceStruct(string value)
        {
            this._privateBackingFieldForProperty_Value = value;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-value.
        /// </summary>
        public string Value
        {
            get { return this._privateBackingFieldForProperty_Value; }
        }


        #region IEquatable<SourceStruct> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the current object is equal to the <paramref name="other" /> parameter; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='other'>
        /// An object to compare with this object.
        /// </param>
        public bool Equals(SourceStruct other)
        {
            return StringComparer.Ordinal.Equals(this.Value, other.Value);
        }

        /// <summary>
        /// Determines whether two specified <see cref='SourceStruct'/> objects have the same value.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the value of <paramref name='left'/> is the same as the value of <paramref name='right'/>; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='left'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        /// <param name='right'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        public static bool operator ==(SourceStruct left, SourceStruct right)
        {
            return EqualityComparer<SourceStruct>.Default.Equals(left, right);
        }

        /// <summary>
        /// Determines whether two specified <see cref='SourceStruct'/> objects have different values.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the value of <paramref name='left'/> is the same as the value of <paramref name='right'/>; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='left'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        /// <param name='right'>
        /// A <see cref='SourceStruct'/> object.
        /// </param>
        public static bool operator !=(SourceStruct left, SourceStruct right)
        {
            return !EqualityComparer<SourceStruct>.Default.Equals(left, right);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the specified object  is equal to the current object; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='obj'>
        /// The object to compare with the current object.
        /// </param>
        public override bool Equals(object obj)
        {
            return obj is SourceStruct
                && this.Equals((SourceStruct)obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        public override int GetHashCode()
        {
            int hash = StringComparer.Ordinal.GetHashCode(Value);
            return hash;
        }

        #endregion

    }

}
