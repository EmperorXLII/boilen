﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;


namespace BoilenEditor.Primitives {

    [ContentProperty( "Matches" )]
    public sealed class MatchedValueConverter : IValueConverter {

        private readonly List<MatchPair> matches_ = new List<MatchPair>( );
        private readonly MatchPair default_ = new MatchPair( );


        public List<MatchPair> Matches {
            get { return this.matches_; }
        }

        public object DefaultResult {
            get { return this.default_.Result; }
            set { this.default_.Result = value; }
        }


        public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) {
            foreach( MatchPair match in this.Matches ) {
                object target = match.GetConvertedValue( value );
                if( object.Equals( value, target ) )
                    return match.GetConvertedResult( targetType );
            }

            return this.default_.GetConvertedResult( targetType );
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) {
            throw new NotSupportedException( );
        }

    }

}
