﻿
// <auto-generated>
//   !!! WARNING !!!
//   !!!   This code was automatically generated from a T4 template.
//   !!!   Do not edit this file directly. Changes to this file will be lost.
//   !!! WARNING !!!
//   
//      Source Template - TemplateFileData.b.tt
// </auto-generated>


using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Documents;


namespace BoilenEditor.Primitives
{

    partial class TemplateFileData
    {

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.TemplateFileData.SourceFile'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.TemplateFileData.SourceFile'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty SourceFileProperty;
#if SL3_0
        private static readonly DependencyProperty GeneratedFilePropertyKey;

        internal static readonly DependencyProperty GeneratedFileProperty;

        private static readonly DependencyProperty SourceFileContentsPropertyKey;

        internal static readonly DependencyProperty SourceFileContentsProperty;

        private static readonly DependencyProperty GeneratedFileContentsPropertyKey;

        internal static readonly DependencyProperty GeneratedFileContentsProperty;

        private static readonly DependencyProperty StatusPropertyKey;

        internal static readonly DependencyProperty StatusProperty;

        private static readonly DependencyProperty CurrentStatusPropertyKey;

        internal static readonly DependencyProperty CurrentStatusProperty;

        private static readonly DependencyProperty CurrentStatusTextPropertyKey;

        internal static readonly DependencyProperty CurrentStatusTextProperty;
#else
        private static readonly DependencyPropertyKey GeneratedFilePropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.TemplateFileData.GeneratedFile'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.TemplateFileData.GeneratedFile'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty GeneratedFileProperty;

        private static readonly DependencyPropertyKey SourceFileContentsPropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.TemplateFileData.SourceFileContents'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.TemplateFileData.SourceFileContents'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty SourceFileContentsProperty;

        private static readonly DependencyPropertyKey GeneratedFileContentsPropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.TemplateFileData.GeneratedFileContents'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.TemplateFileData.GeneratedFileContents'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty GeneratedFileContentsProperty;

        private static readonly DependencyPropertyKey StatusPropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.TemplateFileData.Status'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.TemplateFileData.Status'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StatusProperty;

        private static readonly DependencyPropertyKey CurrentStatusPropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.TemplateFileData.CurrentStatus'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.TemplateFileData.CurrentStatus'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CurrentStatusProperty;

        private static readonly DependencyPropertyKey CurrentStatusTextPropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.TemplateFileData.CurrentStatusText'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.TemplateFileData.CurrentStatusText'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty CurrentStatusTextProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static TemplateFileData()
        {
#if SL3_0
            TemplateFileData.SourceFileProperty = DependencyProperty.Register(
                "SourceFile", typeof(FileData), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileData),
                    new PropertyChangedCallback(TemplateFileData.SourceFileChangedShim)
                )
            );
            TemplateFileData.GeneratedFilePropertyKey = DependencyProperty.Register(
                "GeneratedFile", typeof(FileData), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileData)
                )
            );
            TemplateFileData.GeneratedFileProperty = GeneratedFilePropertyKey;
            TemplateFileData.SourceFileContentsPropertyKey = DependencyProperty.Register(
                "SourceFileContents", typeof(FileContents), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileContents)
                )
            );
            TemplateFileData.SourceFileContentsProperty = SourceFileContentsPropertyKey;
            TemplateFileData.GeneratedFileContentsPropertyKey = DependencyProperty.Register(
                "GeneratedFileContents", typeof(FileContents), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileContents)
                )
            );
            TemplateFileData.GeneratedFileContentsProperty = GeneratedFileContentsPropertyKey;
            TemplateFileData.StatusPropertyKey = DependencyProperty.Register(
                "Status", typeof(ObservableCollection<Inline>), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(ObservableCollection<Inline>)
                )
            );
            TemplateFileData.StatusProperty = StatusPropertyKey;
            TemplateFileData.CurrentStatusPropertyKey = DependencyProperty.Register(
                "CurrentStatus", typeof(Inline), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(Inline)
                )
            );
            TemplateFileData.CurrentStatusProperty = CurrentStatusPropertyKey;
            TemplateFileData.CurrentStatusTextPropertyKey = DependencyProperty.Register(
                "CurrentStatusText", typeof(string), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(string)
                )
            );
            TemplateFileData.CurrentStatusTextProperty = CurrentStatusTextPropertyKey;
#else
            TemplateFileData.SourceFileProperty = DependencyProperty.Register(
                "SourceFile", typeof(FileData), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileData),
                    new PropertyChangedCallback(TemplateFileData.SourceFileChangedShim)
                )
            );
            TemplateFileData.GeneratedFilePropertyKey = DependencyProperty.RegisterReadOnly(
                "GeneratedFile", typeof(FileData), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileData)
                )
            );
            TemplateFileData.GeneratedFileProperty = GeneratedFilePropertyKey.DependencyProperty;
            TemplateFileData.SourceFileContentsPropertyKey = DependencyProperty.RegisterReadOnly(
                "SourceFileContents", typeof(FileContents), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileContents)
                )
            );
            TemplateFileData.SourceFileContentsProperty = SourceFileContentsPropertyKey.DependencyProperty;
            TemplateFileData.GeneratedFileContentsPropertyKey = DependencyProperty.RegisterReadOnly(
                "GeneratedFileContents", typeof(FileContents), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(FileContents)
                )
            );
            TemplateFileData.GeneratedFileContentsProperty = GeneratedFileContentsPropertyKey.DependencyProperty;
            TemplateFileData.StatusPropertyKey = DependencyProperty.RegisterReadOnly(
                "Status", typeof(ObservableCollection<Inline>), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(ObservableCollection<Inline>)
                )
            );
            TemplateFileData.StatusProperty = StatusPropertyKey.DependencyProperty;
            TemplateFileData.CurrentStatusPropertyKey = DependencyProperty.RegisterReadOnly(
                "CurrentStatus", typeof(Inline), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(Inline)
                )
            );
            TemplateFileData.CurrentStatusProperty = CurrentStatusPropertyKey.DependencyProperty;
            TemplateFileData.CurrentStatusTextPropertyKey = DependencyProperty.RegisterReadOnly(
                "CurrentStatusText", typeof(string), typeof(TemplateFileData),
                new PropertyMetadata(
                    default(string)
                )
            );
            TemplateFileData.CurrentStatusTextProperty = CurrentStatusTextPropertyKey.DependencyProperty;
#endif

            TemplateFileData.InitializeType();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='TemplateFileData'/> class.
        /// </summary>
        public TemplateFileData()
        {
            this.Status = new ObservableCollection<Inline>( );

            this.InitializeInstance();
        }

        static partial void InitializeType();

        partial void InitializeInstance();


        /// <summary>
        /// Gets or sets the source Boilen template file.
        /// </summary>
        public FileData SourceFile
        {
            get { return (FileData)this.GetValue(TemplateFileData.SourceFileProperty); }
            set { this.SetValue(TemplateFileData.SourceFileProperty, value); }
        }

        private static void SourceFileChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TemplateFileData self = (TemplateFileData)d;
            self.OnSourceFileChanged();
        }

        /// <summary>
        /// Gets the file generated from the <see cref='TemplateFileData.SourceFile'/>.
        /// </summary>
        public FileData GeneratedFile
        {
            get { return (FileData)this.GetValue(TemplateFileData.GeneratedFileProperty); }
            private set { this.SetValue(TemplateFileData.GeneratedFilePropertyKey, value); }
        }

        /// <summary>
        /// Gets the contents of the <see cref='TemplateFileData.SourceFile'/>.
        /// </summary>
        public FileContents SourceFileContents
        {
            get { return (FileContents)this.GetValue(TemplateFileData.SourceFileContentsProperty); }
            private set { this.SetValue(TemplateFileData.SourceFileContentsPropertyKey, value); }
        }

        /// <summary>
        /// Gets the contents of the <see cref='TemplateFileData.GeneratedFile'/>.
        /// </summary>
        public FileContents GeneratedFileContents
        {
            get { return (FileContents)this.GetValue(TemplateFileData.GeneratedFileContentsProperty); }
            private set { this.SetValue(TemplateFileData.GeneratedFileContentsPropertyKey, value); }
        }

        /// <summary>
        /// Gets the current status.
        /// </summary>
        public ObservableCollection<Inline> Status
        {
            get { return (ObservableCollection<Inline>)this.GetValue(TemplateFileData.StatusProperty); }
            private set { this.SetValue(TemplateFileData.StatusPropertyKey, value); }
        }

        /// <summary>
        /// Gets the current status.
        /// </summary>
        public Inline CurrentStatus
        {
            get { return (Inline)this.GetValue(TemplateFileData.CurrentStatusProperty); }
            private set { this.SetValue(TemplateFileData.CurrentStatusPropertyKey, value); }
        }

        /// <summary>
        /// Gets the current status.
        /// </summary>
        public string CurrentStatusText
        {
            get { return (string)this.GetValue(TemplateFileData.CurrentStatusTextProperty); }
            private set { this.SetValue(TemplateFileData.CurrentStatusTextPropertyKey, value); }
        }

    }

}
