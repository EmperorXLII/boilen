
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;
        private readonly string _privateBackingFieldForProperty_BufferProperty;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified bufferProperty and intProperty.
        /// </summary>
        /// <param name='bufferProperty'>
        /// Test-description-for-bufferProperty.
        /// </param>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public SourceStruct(string bufferProperty, int intProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;
            this._privateBackingFieldForProperty_BufferProperty = bufferProperty;

            this.InitializeInstance();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified bufferProperty.
        /// </summary>
        /// <param name='bufferProperty'>
        /// Test-description-for-bufferProperty.
        /// </param>
        public SourceStruct(string bufferProperty)
            : this(bufferProperty, 1) { }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

        /// <summary>
        /// Gets test-description-for-bufferProperty.
        /// </summary>
        public string BufferProperty
        {
            get { return this._privateBackingFieldForProperty_BufferProperty; }
        }

    }

}
