
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.VirtualCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.VirtualCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty VirtualCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.VirtualCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceDependencyObject.VirtualCallbacksChangedShim)
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-virtualCallbacks.
        /// </summary>
        public int VirtualCallbacks
        {
            get { return (int)this.GetValue(SourceDependencyObject.VirtualCallbacksProperty); }
            set { this.SetValue(SourceDependencyObject.VirtualCallbacksProperty, value); }
        }

        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            self.OnVirtualCallbacksChanged((int)e.OldValue, (int)e.NewValue);
        }

        /// <summary>
        /// Called when the <see cref="SourceDependencyObject.VirtualCallbacks" /> property changes.
        /// </summary>
        /// <param name='oldValue'>
        /// Old value of the <see cref="SourceDependencyObject.VirtualCallbacks" /> property.
        /// </param>
        /// <param name='newValue'>
        /// New value of the <see cref="SourceDependencyObject.VirtualCallbacks" /> property.
        /// </param>
        protected virtual void OnVirtualCallbacksChanged(int oldValue, int newValue)
        {
            this.OnVirtualCallbacksChanged();
        }

        partial void OnVirtualCallbacksChanged();

    }

}
