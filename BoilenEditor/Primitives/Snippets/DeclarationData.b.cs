﻿
// <auto-generated>
//   !!! WARNING !!!
//   !!!   This code was automatically generated from a T4 template.
//   !!!   Do not edit this file directly. Changes to this file will be lost.
//   !!! WARNING !!!
//   
//      Source Template - DeclarationData.b.tt
// </auto-generated>


using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using BoilenEditor.Restricted;


namespace BoilenEditor.Primitives.Snippets
{

    partial class DeclarationData :
        INotifyPropertyChanged
    {

        private readonly MethodInfo _privateBackingFieldForProperty_Method;
        private readonly string _privateBackingFieldForProperty_Category;
        private readonly string _privateBackingFieldForProperty_DisplayName;
        private readonly string _privateBackingFieldForProperty_FullName;
        private readonly ReadOnlyCollection<ArgumentData> _privateBackingFieldForProperty_Arguments;
        private readonly ReadOnlyCollection<SetMemberGroupData> _privateBackingFieldForProperty_Members;


        /// <summary>
        /// Initializes a new instance of the <see cref='DeclarationData'/> class with the specified method, category, and displayName.
        /// </summary>
        /// <param name='method'>
        /// The method that generates this member.
        /// </param>
        /// <param name='category'>
        /// The category of the declaration.
        /// </param>
        /// <param name='displayName'>
        /// The display name of the declaration.
        /// </param>
        public DeclarationData(MethodInfo method, string category, string displayName)
        {
            this._privateBackingFieldForProperty_Method = method;
            this._privateBackingFieldForProperty_Category = category;
            this._privateBackingFieldForProperty_DisplayName = displayName;

            this._privateBackingFieldForProperty_FullName = GetFullName( method );
            this._privateBackingFieldForProperty_Arguments = GetArguments( method );
            this._privateBackingFieldForProperty_Members = GetMembers( method );

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets the method that generates this member.
        /// </summary>
        public MethodInfo Method
        {
            get { return this._privateBackingFieldForProperty_Method; }
        }

        /// <summary>
        /// Gets the category of the declaration.
        /// </summary>
        public string Category
        {
            get { return this._privateBackingFieldForProperty_Category; }
        }

        /// <summary>
        /// Gets the display name of the declaration.
        /// </summary>
        public string DisplayName
        {
            get { return this._privateBackingFieldForProperty_DisplayName; }
        }

        /// <summary>
        /// Gets the full name of the declaration method.
        /// </summary>
        public string FullName
        {
            get { return this._privateBackingFieldForProperty_FullName; }
        }

        /// <summary>
        /// Gets the snippet members of the declaration.
        /// </summary>
        public ReadOnlyCollection<ArgumentData> Arguments
        {
            get { return this._privateBackingFieldForProperty_Arguments; }
        }

        /// <summary>
        /// Gets the snippet members of the declaration.
        /// </summary>
        public ReadOnlyCollection<SetMemberGroupData> Members
        {
            get { return this._privateBackingFieldForProperty_Members; }
        }


        #region INotifyPropertyChanged Members

        private PropertyChangedEventHandler _privateBackingFieldForEvent_PropertyChanged;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add { this._privateBackingFieldForEvent_PropertyChanged += value; }
            remove { this._privateBackingFieldForEvent_PropertyChanged -= value; }
        }

        /// <summary>
        /// Raises the <see cref='DeclarationData.PropertyChanged'/> event.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='e'/> cannot be <see langword='null'/>.
        /// </exception>
        /// <param name='e'>
        /// An instance of <see cref='PropertyChangedEventArgs'/> that contains the event data.
        /// </param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            e.GuardParam("e")
                .NotNull();

            PropertyChangedEventHandler handler = Interlocked.CompareExchange(ref this._privateBackingFieldForEvent_PropertyChanged, null, null);
            if (!object.ReferenceEquals(handler, null))
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref='DeclarationData.PropertyChanged'/> event.
        /// </summary>
        /// <param name='propertyName'>
        /// The name of the property that changed.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Justification = "Supports the PropertyChanged event.")]
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
            this.OnPropertyChanged(e);
        }

        #endregion
        #region Property Names


        #endregion

    }

}
