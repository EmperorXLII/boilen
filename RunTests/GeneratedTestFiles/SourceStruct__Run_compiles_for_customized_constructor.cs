
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private readonly int _privateBackingFieldForProperty_Name;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified name.
        /// </summary>
        /// <param name='name'>
        /// Description.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Category", "ID", Justification = "Justification")]
        internal SourceStruct(int name)
        {
            this._privateBackingFieldForProperty_Name = name;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets description.
        /// </summary>
        public int Name
        {
            get { return this._privateBackingFieldForProperty_Name; }
        }

    }

}
