﻿
using BoilenEditor.Properties;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;


namespace BoilenEditor.Primitives {

    public sealed partial class SettingsData {

        public void Save( ) {
            this.Settings.Save( );
        }

        public void Reload( ) {
            this.Settings.Reload( );
        }


        private static ReadOnlyCollection<SettingPropertyData> GetProperties( Settings settings ) {
            var properties = new List<SettingPropertyData>( );

            // Use "settings.Properties" to ensure property values are initialized.
            foreach( SettingsProperty property in settings.Properties ) {
                string name = property.Name;
#pragma warning disable 168 // Ensure property value is initialized.
                object value = settings[name];
#pragma warning restore 168

                SettingsPropertyValue setting = settings.PropertyValues[name];
                properties.Add( new SettingPropertyData( setting ) );
            }

            return properties.ToReadOnlyCollection( );
        }

    }

}
