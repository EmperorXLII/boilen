
using Xunit;


namespace Boilen.Primitives.Members {

    public class TestMemberGroup {

        [Fact]
        public void Write_succeeds_for_valid_arguments( ) {
            string groupName = "group name";
            string line = "test line";
            string[] expectedLines = new[] {
                "#region " + groupName,
                "",
                line,
                "",
                "#endregion",
                ""
            };
            var writer = new StringBuilderCodeWriter( );
            var group = new MemberGroup( groupName );
            group.Members.Add( new WriteNameMember( line ) );

            group.Write( writer );

            string[] lines = writer.GetLines( );
            AssertExtensions.EqualCollection( lines, expectedLines );
        }

    }

}
