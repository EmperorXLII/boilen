
using System;
using System.Collections.Generic;
using TypeAlias = System.String;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private TypeAlias _privateBackingFieldForProperty_Property;


        /// <summary>
        /// Gets or sets test-description-for-property.
        /// </summary>
        public TypeAlias Property
        {
            get { return this._privateBackingFieldForProperty_Property; }
            set
            {
                this.PropertyCoerce(ref value);

                if (!EqualityComparer<TypeAlias>.Default.Equals(this._privateBackingFieldForProperty_Property, value))
                {
                    this.PropertyChanging(value);
                    TypeAlias oldValue = this._privateBackingFieldForProperty_Property;
                    this._privateBackingFieldForProperty_Property = value;
                    this.PropertyChanged(oldValue);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void PropertyCoerce(ref TypeAlias value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void PropertyChanging(TypeAlias newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void PropertyChanged(TypeAlias oldValue);

    }

}
