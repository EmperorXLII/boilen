
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.VirtualCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.VirtualCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty VirtualCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.VirtualCallbacksChangedShim)
                )
            );
#else
            SourceFreezable.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.VirtualCallbacksChangedShim)
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-virtualCallbacks.
        /// </summary>
        public int VirtualCallbacks
        {
            get { return (int)this.GetValue(SourceFreezable.VirtualCallbacksProperty); }
            set { this.SetValue(SourceFreezable.VirtualCallbacksProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_VirtualCallbacks_RevertingDependencyPropertyChange_SL;
#endif

        /// <summary>
        /// Called when the <see cref="SourceFreezable.VirtualCallbacks" /> property changes.
        /// </summary>
        /// <param name='oldValue'>
        /// Old value of the <see cref="SourceFreezable.VirtualCallbacks" /> property.
        /// </param>
        /// <param name='newValue'>
        /// New value of the <see cref="SourceFreezable.VirtualCallbacks" /> property.
        /// </param>
        protected virtual void OnVirtualCallbacksChanged(int oldValue, int newValue)
        {
            this.OnVirtualCallbacksChanged();
        }

        partial void OnVirtualCallbacksChanged();
#if SL3_0
        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_VirtualCallbacks_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_VirtualCallbacks_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.VirtualCallbacksProperty, e.OldValue);
                self._privateBackingFieldForProperty_VirtualCallbacks_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnVirtualCallbacksChanged((int)e.OldValue, (int)e.NewValue);
            self.OnSubpropertyChanged();
        }
#else
        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            self.OnVirtualCallbacksChanged((int)e.OldValue, (int)e.NewValue);
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
