
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

#if SL3_0
    using FrameworkContentElement = FrameworkElement;
#endif

    partial class SourceClass : FrameworkContentElement
    {

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceClass()
        {
#if !SL3_0
            FrameworkContentElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceClass), new FrameworkPropertyMetadata(typeof(SourceClass)));
#endif

            SourceClass.InitializeType();
        }

        static partial void InitializeType();

    }

}
