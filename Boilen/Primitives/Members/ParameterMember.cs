﻿
using System;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for a parameter declaration.
    /// </summary>
    public sealed class ParameterMember : TypedMember {

        /// <inheritdoc/>
        public override bool InlineDoc { get { return false; } }

        /// <inheritdoc/>
        public override Extensions UsedExtensions { get { return Extensions.None; } }


        /// <summary>
        /// Initializes a new <see cref="ParameterMember"/> instance.
        /// </summary>
        /// <param name="name">The name of the parameter.</param>
        /// <param name="type">The type of the parameter.</param>
        public ParameterMember( string name, string type )
            : base( name, type, true ) {
        }


        /// <inheritdoc/>
        protected override void WriteCore( ICodeWriter writer ) {
            writer.Write( "{0} {1}", this.Type, this.Name );
        }

    }

}
