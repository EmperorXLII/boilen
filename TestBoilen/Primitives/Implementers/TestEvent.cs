﻿
using Boilen.Primitives.CodeGeneration;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xunit.Extensions;


namespace Boilen.Primitives.Implementers {

    public class TestEvent : TestImplementers {

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_Event( Type sourceType ) {
            var eci = MemberTestInfo.Create<int>( "state" );

            var pt = Partial.Type( sourceType )
                .AddEvent<EventHandler>( eci.ParameterName, eci.Description )
                ;
            var members = Compile.PartialType( pt );
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_Event_with_external_documentation( Type sourceType ) {
            var eci = MemberTestInfo.Create<int>( "state" );

            using( ChangeExternalDocumentationPrefix( ) ) {
                var pt = Partial.Type( sourceType )
                    .AddEvent<EventHandler>( eci.ParameterName )
                    ;
                var members = Compile.PartialType( pt );
            }
        }

        [Theory]
        [PropertyData( "SourceTypes" )]
        public void code_compiles_for_1_Event_with_args_construction_disabled( Type sourceType ) {
            var eci = MemberTestInfo.Create<int>( "state" );

            var pt = Partial.Type( sourceType )
                .AddEvent<MouseEventHandler>( eci.ParameterName, eci.Description, p => p
                    .SetEnableArgsConvenienceMethod( false )
                    .SetAccessibility( Accessibility.Internal )
                )
                ;
            var members = Compile.PartialType( pt );
        }


        public static IEnumerable<object[]> SourceTypes { get { return SourceTypesCore; } }

    }

}
