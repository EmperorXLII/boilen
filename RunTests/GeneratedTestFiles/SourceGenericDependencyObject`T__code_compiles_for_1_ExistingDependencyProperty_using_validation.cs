
using System;
using System.Windows;
using System.Windows.Controls;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.Orientation" /> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject{T}.Orientation" /> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty OrientationProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.OrientationProperty = DependencyProperty.Register(
                "Orientation", typeof(Orientation), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    Orientation.Vertical,
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.OrientationChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.OrientationProperty = StackPanel.OrientationProperty.AddOwner(typeof(SourceGenericDependencyObject<T>));
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets a value that indicates the dimension by which child elements are stacked.
        /// </summary>
        /// <returns>
        /// The <see cref="System.Windows.Controls.Orientation" /> of child content.
        /// </returns>
        public Orientation Orientation
        {
            get { return (Orientation)this.GetValue(SourceGenericDependencyObject<T>.OrientationProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.OrientationProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL;

        private static void OrientationChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceGenericDependencyObject<T> self = (SourceGenericDependencyObject<T>)d;
            if (self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL)
                return;

            object value = e.NewValue;
            bool isValid = DependencyPropertyCallbacks.IsValidEnum<Orientation>(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceGenericDependencyObject<T>.OrientationProperty, e.OldValue);
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'Orientation'.", value);
            }
        }
#endif

    }

}
