﻿
using BoilenEditor.Primitives;
using BoilenEditor.Properties;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;


namespace BoilenEditor {

    public partial class App : Application {
        private static readonly ReadOnlyCollection<string> EmptyArguments = new ReadOnlyCollection<string>( new string[0] );

        private static ReadOnlyCollection<string> arguments_ = EmptyArguments;


        public App( ) {
            if( !Debugger.IsAttached )
                AppDomain.CurrentDomain.UnhandledException += DisplayUnhandledException;

            this.UpdateFontSize( Settings.Default.FontSize );
        }


        public static App Active {
            get { return (App)App.Current; }
        }

        public static ReadOnlyCollection<string> Arguments {
            get { return arguments_; }
        }


        public void UpdateFontSize( double fontSize ) {
            this.Resources["FontSize"] = fontSize;
        }

        protected override void OnStartup( StartupEventArgs e ) {
            base.OnStartup( e );
            arguments_ = e.Args.ToReadOnlyCollection( );
        }

        private static void DisplayUnhandledException( object sender, UnhandledExceptionEventArgs e ) {
            Exception ex = (Exception)e.ExceptionObject;
            while( ex.InnerException.HasValue( ) )
                ex = ex.InnerException;

            string message = ex.GetType( ).FullName + ":" + Environment.NewLine + ex.Message;
#if DEBUG
            message += Environment.NewLine + Environment.NewLine + ex.StackTrace;
#endif
            MessageBox.Show( message, "Unhandled Exception", MessageBoxButton.OK, MessageBoxImage.Error );
        }
    }

}
