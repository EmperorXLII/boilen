
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct
    {

        private readonly bool _privateBackingFieldForProperty_IntProperty;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceStruct'/> struct with the specified intProperty.
        /// </summary>
        /// <param name='intProperty'>
        /// Test-description-for-intProperty.
        /// </param>
        public SourceStruct(bool intProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-intProperty.
        /// </summary>
        public bool IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

    }

}
