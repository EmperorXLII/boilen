
using System;
using System.Collections.Generic;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass :
        IEquatable<SourceClass>
    {

        private readonly string _privateBackingFieldForProperty_Value;


        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class with the specified value.
        /// </summary>
        /// <param name='value'>
        /// Test-description-for-value.
        /// </param>
        public SourceClass(string value)
        {
            this._privateBackingFieldForProperty_Value = value;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <summary>
        /// Gets test-description-for-value.
        /// </summary>
        public string Value
        {
            get { return this._privateBackingFieldForProperty_Value; }
        }


        #region IEquatable<SourceClass> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the current object is equal to the <paramref name="other" /> parameter; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='other'>
        /// An object to compare with this object.
        /// </param>
        public bool Equals(SourceClass other)
        {
            return !object.ReferenceEquals(other, null)
                && this.GetType() == other.GetType()
                && EqualityComparer<string>.Default.Equals(this.Value, other.Value)
                && this.EqualsCore(other);
        }

        /// <summary>
        /// Determines whether two specified <see cref='SourceClass'/> objects have the same value.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the value of <paramref name='left'/> is the same as the value of <paramref name='right'/>; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='left'>
        /// A <see cref='SourceClass'/> object or <see langword='null'/>.
        /// </param>
        /// <param name='right'>
        /// A <see cref='SourceClass'/> object or <see langword='null'/>.
        /// </param>
        public static bool operator ==(SourceClass left, SourceClass right)
        {
            return EqualityComparer<SourceClass>.Default.Equals(left, right);
        }

        /// <summary>
        /// Determines whether two specified <see cref='SourceClass'/> objects have different values.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the value of <paramref name='left'/> is the same as the value of <paramref name='right'/>; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='left'>
        /// A <see cref='SourceClass'/> object or <see langword='null'/>.
        /// </param>
        /// <param name='right'>
        /// A <see cref='SourceClass'/> object or <see langword='null'/>.
        /// </param>
        public static bool operator !=(SourceClass left, SourceClass right)
        {
            return !EqualityComparer<SourceClass>.Default.Equals(left, right);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <returns>
        /// <see langword='true'/> if the specified object  is equal to the current object; otherwise, <see langword='false'/>.
        /// </returns>
        /// <param name='obj'>
        /// The object to compare with the current object.
        /// </param>
        public sealed override bool Equals(object obj)
        {
            return this.Equals(obj as SourceClass);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        public sealed override int GetHashCode()
        {
            int hash = EqualityComparer<string>.Default.GetHashCode(this.Value);
            hash = ((hash << 5) + hash) ^ this.GetHashCodeCore();
            return hash;
        }

        #endregion

    }

}
