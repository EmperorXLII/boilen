
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass :
        INotifyPropertyChanged
    {

        private object _privateBackingFieldForProperty_Mutable;


        /// <include file='prefix/SourceClass.xml' path='/doc/member[@name="Mutable"]/*' />
        public object Mutable
        {
            get { return this._privateBackingFieldForProperty_Mutable; }
            set
            {
                this.MutableCoerce(ref value);

                if (!EqualityComparer<object>.Default.Equals(this._privateBackingFieldForProperty_Mutable, value))
                {
                    this.MutableChanging(value);
                    object oldValue = this._privateBackingFieldForProperty_Mutable;
                    this._privateBackingFieldForProperty_Mutable = value;
                    this.MutableChanged(oldValue);
                    this.OnPropertyChanged(MutablePropertyName);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void MutableCoerce(ref object value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void MutableChanging(object newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void MutableChanged(object oldValue);


        #region INotifyPropertyChanged Members

        private PropertyChangedEventHandler _privateBackingFieldForEvent_PropertyChanged;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add { this._privateBackingFieldForEvent_PropertyChanged += value; }
            remove { this._privateBackingFieldForEvent_PropertyChanged -= value; }
        }

        /// <summary>
        /// Raises the <see cref='SourceClass.PropertyChanged'/> event.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='e'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='e'>
        /// An instance of <see cref='PropertyChangedEventArgs'/> that contains the event data.
        /// </param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            e.GuardParam("e")
                .NotNull();

            PropertyChangedEventHandler handler = Interlocked.CompareExchange(ref this._privateBackingFieldForEvent_PropertyChanged, null, null);
            if (!object.ReferenceEquals(handler, null))
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref='SourceClass.PropertyChanged'/> event.
        /// </summary>
        /// <param name='propertyName'>
        /// The name of the property that changed.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Justification = "Supports the PropertyChanged event.")]
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
            this.OnPropertyChanged(e);
        }

        #endregion
        #region Property Names

        /// <summary>
        /// The name of the <see cref='SourceClass.Mutable'/> property.
        /// </summary>
        public const string MutablePropertyName = "Mutable";

        #endregion

    }

}
