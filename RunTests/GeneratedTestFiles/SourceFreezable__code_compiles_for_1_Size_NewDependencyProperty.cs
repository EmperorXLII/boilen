
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Size'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Size'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty SizeProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.SizeProperty = DependencyProperty.Register(
                "Size", typeof(Size), typeof(SourceFreezable),
                new PropertyMetadata(
                    new Size(5, double.NaN),
                    new PropertyChangedCallback(SourceFreezable.SizeChangedShim)
                )
            );
#else
            SourceFreezable.SizeProperty = DependencyProperty.Register(
                "Size", typeof(Size), typeof(SourceFreezable),
                new PropertyMetadata(
                    new Size(5, double.NaN)
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-size.
        /// </summary>
        public Size Size
        {
            get { return (Size)this.GetValue(SourceFreezable.SizeProperty); }
            set { this.SetValue(SourceFreezable.SizeProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Size_RevertingDependencyPropertyChange_SL;

        private static void SizeChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_Size_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_Size_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.SizeProperty, e.OldValue);
                self._privateBackingFieldForProperty_Size_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
