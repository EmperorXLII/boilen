
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Fill'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.Fill'/> dependency property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty FillProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.FillProperty = DependencyProperty.Register(
                "Fill", typeof(Brush), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    (Brush)null
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.FillProperty = Shape.FillProperty.AddOwner(typeof(SourceGenericDependencyObject<T>));
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <include file='prefix/SourceGenericDependencyObject`T.xml' path='/doc/member[@name="Fill"]/*' />
        public Brush Fill
        {
            get { return (Brush)this.GetValue(SourceGenericDependencyObject<T>.FillProperty); }
            set { this.SetValue(SourceGenericDependencyObject<T>.FillProperty, value); }
        }

    }

}
