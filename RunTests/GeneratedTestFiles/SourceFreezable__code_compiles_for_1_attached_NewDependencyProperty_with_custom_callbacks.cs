
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.CustomCallbacks'/> attached property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.CustomCallbacks'/> attached property.
        /// </returns>
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.CustomCallbacksProperty = DependencyProperty.RegisterAttached(
                "CustomCallbacks", typeof(int), typeof(SourceFreezable),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.CustomCallbacksChangedShim)
                )
            );
#else
            SourceFreezable.CustomCallbacksProperty = DependencyProperty.RegisterAttached(
                "CustomCallbacks", typeof(int), typeof(SourceFreezable),
                new UIPropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceFreezable.CustomChangedHandler),
                    new CoerceValueCallback(SourceFreezable.CustomCoerceHandler)
                ),
                new ValidateValueCallback(SourceFreezable.CustomValidateHandler)
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.CustomCallbacks" /> attached property for a specified <see cref="System.Windows.UIElement" />.
        /// </summary>
        /// <returns>
        /// The <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.CustomCallbacks" /> property value for the element.
        /// </returns>
        /// <remarks>
        /// Gets test-description-for-customCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element from which the property value is read.
        /// </param>
        public static int GetCustomCallbacks(UIElement element)
        {
            element.GuardParam("element")
                .NotNull();

            return (int)element.GetValue(SourceFreezable.CustomCallbacksProperty);
        }

        /// <summary>
        /// Sets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.CustomCallbacks" /> attached property to a specified element.
        /// </summary>
        /// <remarks>
        /// Sets test-description-for-customCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element to which the attached property is written.
        /// </param>
        /// <param name='value'>
        /// The needed <see cref="int" /> value.
        /// </param>
        public static void SetCustomCallbacks(UIElement element, int value)
        {
            element.GuardParam("element")
                .NotNull();

            element.SetValue(SourceFreezable.CustomCallbacksProperty, value);
        }
#if SL3_0
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            object value = e.NewValue;
            object coercedValue = SourceFreezable.CustomCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                d.SetValue(SourceFreezable.CustomCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = SourceFreezable.CustomValidateHandler(value);
            if (!isValid)
            {
                d.SetValue(SourceFreezable.CustomCallbacksProperty, e.OldValue);
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'CustomCallbacks'.", value);
            }

            SourceFreezable.CustomChangedHandler(d, e);
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
