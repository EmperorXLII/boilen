﻿
using System;


namespace Boilen {

    /// <summary>
    /// Specifies the kinds of member inheritance.
    /// </summary>
    public enum Inheritance {

        None = 0,
        Virtual,
        Override,
        New,

    }

}
