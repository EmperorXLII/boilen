﻿
using System;


// Used for testing code generation.
namespace Boilen.Primitives.CodeGeneration {

    public partial class BaseClass : IEquatable<BaseClass> {

        private readonly int requiredProperty_;
        private readonly int optionalProperty_;

        public int RequiredProperty {
            get { return this.requiredProperty_; }
        }

        public int OptionalProperty {
            get { return this.optionalProperty_; }
        }

        public BaseClass( int requiredProperty )
            : this( requiredProperty, default( int ) ) { }

        public BaseClass( int requiredProperty, int optionalProperty ) {
            this.requiredProperty_ = requiredProperty;
            this.optionalProperty_ = optionalProperty;
        }

        public virtual bool Equals( BaseClass other ) {
            return other != null
                && RequiredProperty == other.RequiredProperty;
        }
    }

}
