
using System;
using System.Windows;
using Boilen.Freezables;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.VirtualCallbacks'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.VirtualCallbacks'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty VirtualCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
#if SL3_0
            SourceDependencyObject.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    (new FreezableImpl()).SafeFreeze(),
                    new PropertyChangedCallback(SourceDependencyObject.VirtualCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
            SourceDependencyObject.VirtualCallbacksProperty = DependencyProperty.Register(
                "VirtualCallbacks", typeof(FreezableImpl), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    new FreezableImpl(),
                    new PropertyChangedCallback(SourceDependencyObject.VirtualCallbacksChangedShim)
                )
            );
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-virtualCallbacks.
        /// </summary>
        public FreezableImpl VirtualCallbacks
        {
            get { return (FreezableImpl)this.GetValue(SourceDependencyObject.VirtualCallbacksProperty); }
            set { this.SetValue(SourceDependencyObject.VirtualCallbacksProperty, value); }
        }

        /// <summary>
        /// Called when the <see cref="SourceDependencyObject.VirtualCallbacks" /> property changes.
        /// </summary>
        /// <param name='oldValue'>
        /// Old value of the <see cref="SourceDependencyObject.VirtualCallbacks" /> property.
        /// </param>
        /// <param name='newValue'>
        /// New value of the <see cref="SourceDependencyObject.VirtualCallbacks" /> property.
        /// </param>
        protected virtual void OnVirtualCallbacksChanged(FreezableImpl oldValue, FreezableImpl newValue)
        {
            this.OnVirtualCallbacksChanged();
        }

        partial void OnVirtualCallbacksChanged();
#if SL3_0
        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            object localValue = self.ReadLocalValue(VirtualCallbacksProperty);
            if (object.ReferenceEquals(localValue, DependencyProperty.UnsetValue))
                e.NewValue.SafeFreeze();

            e.OldValue.SafeSubpropertyChanged(self.VirtualCallbacksSubpropertyChanged_SL, false);
            e.NewValue.SafeSubpropertyChanged(self.VirtualCallbacksSubpropertyChanged_SL, true);

            self.OnVirtualCallbacksChanged((FreezableImpl)e.OldValue, (FreezableImpl)e.NewValue);
        }

        private void VirtualCallbacksSubpropertyChanged_SL(object sender, EventArgs e)
        {
            FreezableImpl value = this.VirtualCallbacks;
            this.OnVirtualCallbacksChanged(value, value);
        }
#else
        private static void VirtualCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceDependencyObject self = (SourceDependencyObject)d;
            self.OnVirtualCallbacksChanged((FreezableImpl)e.OldValue, (FreezableImpl)e.NewValue);
        }
#endif

    }

}
