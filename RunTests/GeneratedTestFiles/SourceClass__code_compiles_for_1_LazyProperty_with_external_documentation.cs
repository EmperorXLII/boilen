
using System;
using System.Collections.Generic;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private object _privateBackingFieldForProperty_ObjProperty;


        /// <include file='prefix/SourceClass.xml' path='/doc/member[@name="ObjProperty"]/*' />
        public object ObjProperty
        {
            get
            {
                if (object.ReferenceEquals(this._privateBackingFieldForProperty_ObjProperty, null))
                {
                    this.ObjProperty = new object();
                }

                return this._privateBackingFieldForProperty_ObjProperty;
            }
            set
            {
                this.ObjPropertyCoerce(ref value);

                if (!EqualityComparer<object>.Default.Equals(this._privateBackingFieldForProperty_ObjProperty, value))
                {
                    this.ObjPropertyChanging(value);
                    object oldValue = this._privateBackingFieldForProperty_ObjProperty;
                    this._privateBackingFieldForProperty_ObjProperty = value;
                    this.ObjPropertyChanged(oldValue);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void ObjPropertyCoerce(ref object value);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void ObjPropertyChanging(object newValue);

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void ObjPropertyChanged(object oldValue);

    }

}
