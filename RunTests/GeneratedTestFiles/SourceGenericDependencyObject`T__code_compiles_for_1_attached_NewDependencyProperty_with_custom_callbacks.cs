
using System;
using System.Windows;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceGenericDependencyObject<T>
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks'/> attached property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks'/> attached property.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Part of standard property implementation.")]
        public static readonly DependencyProperty CustomCallbacksProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceGenericDependencyObject()
        {
#if SL3_0
            SourceGenericDependencyObject<T>.CustomCallbacksProperty = DependencyProperty.RegisterAttached(
                "CustomCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new PropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.CustomCallbacksChangedShim)
                )
            );
#else
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceGenericDependencyObject<T>), new FrameworkPropertyMetadata(typeof(SourceGenericDependencyObject<T>)));
            SourceGenericDependencyObject<T>.CustomCallbacksProperty = DependencyProperty.RegisterAttached(
                "CustomCallbacks", typeof(int), typeof(SourceGenericDependencyObject<T>),
                new UIPropertyMetadata(
                    default(int),
                    new PropertyChangedCallback(SourceGenericDependencyObject<T>.CustomChangedHandler),
                    new CoerceValueCallback(SourceGenericDependencyObject<T>.CustomCoerceHandler)
                ),
                new ValidateValueCallback(SourceGenericDependencyObject<T>.CustomValidateHandler)
            );
#endif

            SourceGenericDependencyObject<T>.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks" /> attached property for a specified <see cref="System.Windows.UIElement" />.
        /// </summary>
        /// <returns>
        /// The <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks" /> property value for the element.
        /// </returns>
        /// <remarks>
        /// Gets test-description-for-customCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element from which the property value is read.
        /// </param>
        public static int GetCustomCallbacks(UIElement element)
        {
            element.GuardParam("element")
                .NotNull();

            return (int)element.GetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty);
        }

        /// <summary>
        /// Sets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceGenericDependencyObject`1.CustomCallbacks" /> attached property to a specified element.
        /// </summary>
        /// <remarks>
        /// Sets test-description-for-customCallbacks.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element to which the attached property is written.
        /// </param>
        /// <param name='value'>
        /// The needed <see cref="int" /> value.
        /// </param>
        public static void SetCustomCallbacks(UIElement element, int value)
        {
            element.GuardParam("element")
                .NotNull();

            element.SetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty, value);
        }
#if SL3_0
        private static void CustomCallbacksChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            object value = e.NewValue;
            object coercedValue = SourceGenericDependencyObject<T>.CustomCoerceHandler(d, value);
            if (!object.Equals(coercedValue, value))
            {
                d.SetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty, coercedValue);
                return;
            }

            bool isValid = SourceGenericDependencyObject<T>.CustomValidateHandler(value);
            if (!isValid)
            {
                d.SetValue(SourceGenericDependencyObject<T>.CustomCallbacksProperty, e.OldValue);
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'CustomCallbacks'.", value);
            }

            SourceGenericDependencyObject<T>.CustomChangedHandler(d, e);
        }
#endif

    }

}
