
using System;


namespace Boilen.Primitives.CodeGeneration
{

    partial struct SourceStruct<T>
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;


        /// <include file='prefix/SourceStruct`T.xml' path='/doc/member[@name="SourceStruct(int)"]/*' />
        public SourceStruct(int intProperty)
        {
            this._privateBackingFieldForProperty_IntProperty = intProperty;

            this.InitializeInstance();
        }

        partial void InitializeInstance();


        /// <include file='prefix/SourceStruct`T.xml' path='/doc/member[@name="IntProperty"]/*' />
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

    }

}
