
using System;
using Xunit;


namespace Boilen {

    public class TestAliasTypeAttribute {

        [Fact]
        public void AliasTypeAttribute_given_null_throws_exception( ) {
            Type nullAlias = null;

            Assert.Throws<ArgumentNullException>( ( ) => new AliasTypeAttribute( nullAlias ) );
        }

        [Fact]
        public void AliasTypeAttribute_uses_specified_Alias_value( ) {
            Type expectedAlias = typeof( string );
            var tt = new AliasTypeAttribute( expectedAlias );

            Assert.Equal( tt.Alias, expectedAlias );
        }

    }

}
