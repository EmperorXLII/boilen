
using System;
using System.Windows;
using System.Windows.Controls;


namespace Boilen.Primitives.CodeGeneration
{

    [TemplatePart(Name = "PART_LayoutRoot", Type = typeof(Grid))]
    partial class SourceClass : Control
    {

        private Grid _privateBackingFieldForProperty_LayoutRoot;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceClass()
        {
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceClass), new FrameworkPropertyMetadata(typeof(SourceClass)));
#endif

            SourceClass.InitializeType();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='SourceClass'/> class.
        /// </summary>
        public SourceClass()
        {
#if SL3_0
            this.DefaultStyleKey = typeof(SourceClass);
#endif

            this.InitializeInstance();
        }

        /// <inheritdoc cref='System.Windows.FrameworkElement.OnApplyTemplate'/>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Grid oldLayoutRoot = this.LayoutRoot;
            this._privateBackingFieldForProperty_LayoutRoot = this.GetTemplateChild("PART_LayoutRoot") as Grid;
            this.InitializeLayoutRoot(oldLayoutRoot, this.LayoutRoot);

            this.InitializeTemplateParts();
        }

        static partial void InitializeType();

        partial void InitializeInstance();

        partial void InitializeTemplateParts();


        /// <summary>
        /// Gets test-description-for-LayoutRoot.
        /// </summary>
        private Grid LayoutRoot
        {
            get { return this._privateBackingFieldForProperty_LayoutRoot; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Partial method definitions are not required to use method parameters.")]
        partial void InitializeLayoutRoot(Grid oldLayoutRoot, Grid newLayoutRoot);

    }

}
