﻿
using System;


namespace Boilen {

    /// <summary>
    /// Specifies where the inserted text should be written.
    /// </summary>
    public enum InsertionPoint {

        Usings = 0,
        BeforeTypeDeclaration,
        AfterTypeDeclaration,
        Body,

    }

}
