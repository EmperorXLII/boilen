﻿
using System.Diagnostics;


namespace BoilenEditor.Primitives {

    [DebuggerDisplay( "SearchData: Find={Find}" )]
    public sealed partial class SearchData {

        public bool IsFindOperation {
            get { return this.Replace == null; }
        }

    }

}
