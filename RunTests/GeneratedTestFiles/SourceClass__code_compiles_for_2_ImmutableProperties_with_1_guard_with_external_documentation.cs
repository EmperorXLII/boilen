
using System;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceClass
    {

        private readonly int _privateBackingFieldForProperty_IntProperty;
        private readonly string _privateBackingFieldForProperty_StringProperty;


        /// <include file='prefix/SourceClass.xml' path='/doc/member[@name="SourceClass(int,string)"]/*' />
        public SourceClass(int intProperty, string stringProperty)
        {
            stringProperty.GuardParam("stringProperty")
                .NotNull();

            this._privateBackingFieldForProperty_IntProperty = intProperty;
            this._privateBackingFieldForProperty_StringProperty = stringProperty;

            this.InitializeInstance();
        }

        /// <include file='prefix/SourceClass.xml' path='/doc/member[@name="SourceClass(int)"]/*' />
        public SourceClass(int intProperty)
            : this(intProperty, "default") { }

        partial void InitializeInstance();


        /// <include file='prefix/SourceClass.xml' path='/doc/member[@name="IntProperty"]/*' />
        public int IntProperty
        {
            get { return this._privateBackingFieldForProperty_IntProperty; }
        }

        /// <include file='prefix/SourceClass.xml' path='/doc/member[@name="StringProperty"]/*' />
        public string StringProperty
        {
            get { return this._privateBackingFieldForProperty_StringProperty; }
        }

    }

}
