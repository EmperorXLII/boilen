﻿
using System;


namespace Boilen.Primitives {

    /// <summary>
    /// Represents a code file writer.
    /// </summary>
    public interface ICodeWriter {

        /// <summary>
        /// Increases the indent level by the specified value.
        /// </summary>
        /// <param name="indent">The indent value.</param>
        IDisposable PushIndent( string indent );


        /// <summary>
        /// Writes a string value to a target.
        /// </summary>
        /// <param name="value">The value to write.</param>
        void Write( string value );

        /// <summary>
        /// Formats and writes a string value to a target.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="args">The arguments to the format string.</param>
        void Write( string format, params object[] args );

        /// <summary>
        /// Formats and writes a string value to a target, without indent.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="args">The arguments to the format string.</param>
        void WriteUnindented( string format, params object[] args );


        /// <summary>
        /// Writes a newline to a target.
        /// </summary>
        void WriteLine( );

        /// <summary>
        /// Writes a string value to a target followed by a newline.
        /// </summary>
        /// <param name="value">The value to write.</param>
        void WriteLine( string value );

        /// <summary>
        /// Formats and writes a string value to a target followed by a newline.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="args">The arguments to the format string.</param>
        void WriteLine( string format, params object[] args );

        /// <summary>
        /// Formats and writes a string value to a target followed by a newline, without indent.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="args">The arguments to the format string.</param>
        void WriteLineUnindented( string format, params object[] args );

    }

}
