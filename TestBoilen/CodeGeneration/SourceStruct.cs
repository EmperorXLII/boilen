﻿


// Used for testing code generation.
namespace Boilen.Primitives.CodeGeneration {

    public partial struct SourceStruct {

        private bool EqualsCore( SourceStruct other ) { return default(bool); }

        private int GetHashCodeCore( ) { return default( int ); }

    }

}
