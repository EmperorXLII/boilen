
using System;
using System.Windows;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StateProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
            SourceDependencyObject.StateProperty = DependencyProperty.Register(
                "State", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-state.
        /// </summary>
#if !SL3_0
        [Obsolete("This is obsolete.", false)]
#endif
        public int State
        {
            get { return (int)this.GetValue(SourceDependencyObject.StateProperty); }
            set { this.SetValue(SourceDependencyObject.StateProperty, value); }
        }

    }

}
