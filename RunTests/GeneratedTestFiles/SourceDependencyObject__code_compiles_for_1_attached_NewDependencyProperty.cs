
using System;
using System.Windows;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceDependencyObject
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> attached property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State'/> attached property.
        /// </returns>
        public static readonly DependencyProperty StateProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceDependencyObject()
        {
            SourceDependencyObject.StateProperty = DependencyProperty.RegisterAttached(
                "State", typeof(int), typeof(SourceDependencyObject),
                new PropertyMetadata(
                    default(int)
                )
            );
#if !SL3_0
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SourceDependencyObject), new FrameworkPropertyMetadata(typeof(SourceDependencyObject)));
#endif

            SourceDependencyObject.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State" /> attached property for a specified <see cref="System.Windows.UIElement" />.
        /// </summary>
        /// <returns>
        /// The <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State" /> property value for the element.
        /// </returns>
        /// <remarks>
        /// Gets test-description-for-state.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element from which the property value is read.
        /// </param>
        public static int GetState(DependencyObject element)
        {
            element.GuardParam("element")
                .NotNull();

            return (int)element.GetValue(SourceDependencyObject.StateProperty);
        }

        /// <summary>
        /// Sets the value of the <see cref="P:Boilen.Primitives.CodeGeneration.SourceDependencyObject.State" /> attached property to a specified element.
        /// </summary>
        /// <remarks>
        /// Sets test-description-for-state.
        /// </remarks>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='element'/> is <see langword='null'/>.
        /// </exception>
        /// <param name='element'>
        /// The element to which the attached property is written.
        /// </param>
        /// <param name='value'>
        /// The needed <see cref="int" /> value.
        /// </param>
        public static void SetState(DependencyObject element, int value)
        {
            element.GuardParam("element")
                .NotNull();

            element.SetValue(SourceDependencyObject.StateProperty, value);
        }

    }

}
