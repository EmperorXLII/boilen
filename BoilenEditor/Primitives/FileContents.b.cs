﻿
// <auto-generated>
//   !!! WARNING !!!
//   !!!   This code was automatically generated from a T4 template.
//   !!!   Do not edit this file directly. Changes to this file will be lost.
//   !!! WARNING !!!
//   
//      Source Template - FileContents.b.tt
// </auto-generated>


using System;
using System.Threading;
using System.Windows;
using BoilenEditor.Restricted;


namespace BoilenEditor.Primitives
{

    partial class FileContents
    {

        private readonly FileData _privateBackingFieldForProperty_FileData;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.FileContents.Content'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.FileContents.Content'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty ContentProperty;
        private EventHandler _privateBackingFieldForEvent_ContentChanged;
#if SL3_0
        private static readonly DependencyProperty IsReadingPropertyKey;

        internal static readonly DependencyProperty IsReadingProperty;

        private static readonly DependencyProperty IsModifiedPropertyKey;

        internal static readonly DependencyProperty IsModifiedProperty;

        private static readonly DependencyProperty StatusNamePropertyKey;

        internal static readonly DependencyProperty StatusNameProperty;
#else
        private static readonly DependencyPropertyKey IsReadingPropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.FileContents.IsReading'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.FileContents.IsReading'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty IsReadingProperty;

        private static readonly DependencyPropertyKey IsModifiedPropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.FileContents.IsModified'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.FileContents.IsModified'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty IsModifiedProperty;

        private static readonly DependencyPropertyKey StatusNamePropertyKey;

        /// <summary>
        /// Identifies the <see cref='P:BoilenEditor.Primitives.FileContents.StatusName'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:BoilenEditor.Primitives.FileContents.StatusName'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty StatusNameProperty;
#endif


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static FileContents()
        {
            FileContents.ContentProperty = DependencyProperty.Register(
                "Content", typeof(string), typeof(FileContents),
                new PropertyMetadata(
                    default(string),
                    new PropertyChangedCallback(FileContents.ContentPropertyChanged)
                )
            );
#if SL3_0
            FileContents.IsReadingPropertyKey = DependencyProperty.Register(
                "IsReading", typeof(bool), typeof(FileContents),
                new PropertyMetadata(
                    default(bool)
                )
            );
            FileContents.IsReadingProperty = IsReadingPropertyKey;
            FileContents.IsModifiedPropertyKey = DependencyProperty.Register(
                "IsModified", typeof(bool), typeof(FileContents),
                new PropertyMetadata(
                    default(bool),
                    new PropertyChangedCallback(FileContents.IsModifiedChangedShim)
                )
            );
            FileContents.IsModifiedProperty = IsModifiedPropertyKey;
            FileContents.StatusNamePropertyKey = DependencyProperty.Register(
                "StatusName", typeof(string), typeof(FileContents),
                new PropertyMetadata(
                    default(string)
                )
            );
            FileContents.StatusNameProperty = StatusNamePropertyKey;
#else
            FileContents.IsReadingPropertyKey = DependencyProperty.RegisterReadOnly(
                "IsReading", typeof(bool), typeof(FileContents),
                new PropertyMetadata(
                    default(bool)
                )
            );
            FileContents.IsReadingProperty = IsReadingPropertyKey.DependencyProperty;
            FileContents.IsModifiedPropertyKey = DependencyProperty.RegisterReadOnly(
                "IsModified", typeof(bool), typeof(FileContents),
                new PropertyMetadata(
                    default(bool),
                    new PropertyChangedCallback(FileContents.IsModifiedChangedShim)
                )
            );
            FileContents.IsModifiedProperty = IsModifiedPropertyKey.DependencyProperty;
            FileContents.StatusNamePropertyKey = DependencyProperty.RegisterReadOnly(
                "StatusName", typeof(string), typeof(FileContents),
                new PropertyMetadata(
                    default(string)
                )
            );
            FileContents.StatusNameProperty = StatusNamePropertyKey.DependencyProperty;
#endif

            FileContents.InitializeType();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref='FileContents'/> class with the specified fileData.
        /// </summary>
        /// <param name='fileData'>
        /// The file to edit.
        /// </param>
        public FileContents(FileData fileData)
        {
            this._privateBackingFieldForProperty_FileData = fileData;

            this.InitializeInstance();
        }

        static partial void InitializeType();

        partial void InitializeInstance();


        /// <summary>
        /// Gets the file to edit.
        /// </summary>
        public FileData FileData
        {
            get { return this._privateBackingFieldForProperty_FileData; }
        }

        /// <summary>
        /// Gets or sets the content of the file.
        /// </summary>
        public string Content
        {
            get { return (string)this.GetValue(FileContents.ContentProperty); }
            set { this.SetValue(FileContents.ContentProperty, value); }
        }

        /// <summary>
        /// Gets a value indicating whether the file is being read.
        /// </summary>
        public bool IsReading
        {
            get { return (bool)this.GetValue(FileContents.IsReadingProperty); }
            private set { this.SetValue(FileContents.IsReadingPropertyKey, value); }
        }

        /// <summary>
        /// Gets a value indicating whether <see cref='FileContents.Content'/> has been modified since the last read.
        /// </summary>
        public bool IsModified
        {
            get { return (bool)this.GetValue(FileContents.IsModifiedProperty); }
            private set { this.SetValue(FileContents.IsModifiedPropertyKey, value); }
        }

        private static void IsModifiedChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FileContents self = (FileContents)d;
            self.OnIsModifiedChanged();
        }

        /// <summary>
        /// Gets a friendly file name with an asterisk to indicate a modification.
        /// </summary>
        public string StatusName
        {
            get { return (string)this.GetValue(FileContents.StatusNameProperty); }
            private set { this.SetValue(FileContents.StatusNamePropertyKey, value); }
        }

        /// <summary>
        /// Occurs when <see cref='FileContents.Content'/> is changed.
        /// </summary>
        public event EventHandler ContentChanged
        {
            add { this._privateBackingFieldForEvent_ContentChanged += value; }
            remove { this._privateBackingFieldForEvent_ContentChanged -= value; }
        }

        /// <summary>
        /// Raises the <see cref='FileContents.ContentChanged'/> event.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        /// <paramref name='e'/> cannot be <see langword='null'/>.
        /// </exception>
        /// <param name='e'>
        /// An instance of <see cref='EventArgs'/> that contains the event data.
        /// </param>
        protected virtual void OnContentChanged(EventArgs e)
        {
            e.GuardParam("e")
                .NotNull();

            EventHandler handler = Interlocked.CompareExchange(ref this._privateBackingFieldForEvent_ContentChanged, null, null);
            if (!object.ReferenceEquals(handler, null))
            {
                handler(this, e);
            }
        }

    }

}
