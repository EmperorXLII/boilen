
using System;
using System.Windows;
using Boilen.Freezables;
using Boilen.Guards;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Vaule'/> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref='P:Boilen.Primitives.CodeGeneration.SourceFreezable.Vaule'/> dependency property.
        /// </returns>
        public static readonly DependencyProperty VauleProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.VauleProperty = DependencyProperty.Register(
                "Vaule", typeof(double), typeof(SourceFreezable),
                new PropertyMetadata(
                    double.NaN,
                    new PropertyChangedCallback(SourceFreezable.VauleChangedShim)
                )
            );
#else
            SourceFreezable.VauleProperty = DependencyProperty.Register(
                "Vaule", typeof(double), typeof(SourceFreezable),
                new PropertyMetadata(
                    double.NaN
                )
            );
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets test-description-for-vaule.
        /// </summary>
        public double Vaule
        {
            get { return (double)this.GetValue(SourceFreezable.VauleProperty); }
            set { this.SetValue(SourceFreezable.VauleProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Vaule_RevertingDependencyPropertyChange_SL;

        private static void VauleChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_Vaule_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_Vaule_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.VauleProperty, e.OldValue);
                self._privateBackingFieldForProperty_Vaule_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            self.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
