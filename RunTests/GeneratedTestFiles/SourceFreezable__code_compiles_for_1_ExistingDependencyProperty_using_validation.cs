
using System;
using System.Windows;
using System.Windows.Controls;
using Boilen.Freezables;
using Boilen.Guards;
using Boilen.Validations;


namespace Boilen.Primitives.CodeGeneration
{

    partial class SourceFreezable
    {

        /// <summary>
        /// Identifies the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.Orientation" /> dependency property.
        /// </summary>
        /// <returns>
        /// The identifier for the <see cref="P:Boilen.Primitives.CodeGeneration.SourceFreezable.Orientation" /> dependency property.
        /// </returns>
        public static readonly DependencyProperty OrientationProperty;


        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "AUTOGEN: Initializes static members.")]
        static SourceFreezable()
        {
#if SL3_0
            SourceFreezable.OrientationProperty = DependencyProperty.Register(
                "Orientation", typeof(Orientation), typeof(SourceFreezable),
                new PropertyMetadata(
                    Orientation.Vertical,
                    new PropertyChangedCallback(SourceFreezable.OrientationChangedShim)
                )
            );
#else
            SourceFreezable.OrientationProperty = StackPanel.OrientationProperty.AddOwner(typeof(SourceFreezable));
#endif

            SourceFreezable.InitializeType();
        }

        static partial void InitializeType();


        /// <summary>
        /// Gets or sets a value that indicates the dimension by which child elements are stacked.
        /// </summary>
        /// <returns>
        /// The <see cref="System.Windows.Controls.Orientation" /> of child content.
        /// </returns>
        public Orientation Orientation
        {
            get { return (Orientation)this.GetValue(SourceFreezable.OrientationProperty); }
            set { this.SetValue(SourceFreezable.OrientationProperty, value); }
        }

#if SL3_0
        private bool _privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL;

        private static void OrientationChangedShim(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SourceFreezable self = (SourceFreezable)d;
            if (self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL)
                return;

            if (self.IsFrozen)
            {
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.OrientationProperty, e.OldValue);
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = false;
                self.GuardValue("this").Satisfies(false, "Cannot set a property on object '{0}' because it is in a read-only state.", self);
            }

            object value = e.NewValue;
            bool isValid = DependencyPropertyCallbacks.IsValidEnum<Orientation>(value);
            if (!isValid)
            {
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = true;
                self.SetValue(SourceFreezable.OrientationProperty, e.OldValue);
                self._privateBackingFieldForProperty_Orientation_RevertingDependencyPropertyChange_SL = false;
                value.GuardParam("value").Satisfies(false, "'{0}' is not a valid value for property 'Orientation'.", value);
            }

            self.OnSubpropertyChanged();
        }
#endif


#if SL3_0
        #region IFreezable Members

        /// <summary>
        /// Calls <see cref='IFreezable.Freeze'/> on every freezable property.
        /// </summary>
        protected override void FreezeProperties()
        {
            base.FreezeProperties();
        }

        #endregion
#else
        #region Freezable Members

        /// <inheritdoc cref='System.Windows.Freezable.CreateInstanceCore'/>
        protected override Freezable CreateInstanceCore()
        {
            return new SourceFreezable();
        }

        #endregion
#endif

    }

}
