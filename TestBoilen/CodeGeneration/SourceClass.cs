﻿


// Used for testing code generation.
namespace Boilen.Primitives.CodeGeneration {

    public partial class SourceClass {

        private bool EqualsCore( SourceClass other ) { return default( bool ); }

        private int GetHashCodeCore( ) { return default( int ); }

    }

}
