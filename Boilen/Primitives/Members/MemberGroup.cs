﻿
using System.Collections.Generic;
using System.Linq;


namespace Boilen.Primitives.Members {

    /// <summary>
    /// Writes code for a group of members.
    /// </summary>
    public sealed class MemberGroup : Member {

        private readonly List<Member> members_ = new List<Member>( );


        /// <inheritdoc/>
        public override Extensions UsedExtensions {
            get { return Util.GetUsedExtensions( this.Members ); }
        }

        /// <summary>
        /// Gets the list of <see cref="Member"/> objects in the group.
        /// </summary>
        public IList<Member> Members { get { return this.members_; } }


        /// <summary>
        /// Initializes a new <see cref="MemberGroup"/> instance.
        /// </summary>
        /// <param name="name">The name of the group.</param>
        public MemberGroup( string name )
            : base( name ) { }


        /// <inheritdoc/>
        protected override void WriteCore( ICodeWriter writer ) {
            Ensure.Satisfies( !this.Members.OfType<InitializationMember>( ).Any( ), "Cannot write initializers in nested group." );

            using( Enclose.Region( writer, this.Name ) ) {
                WriteMembers( writer, this.Members );
            }
        }

    }

}
